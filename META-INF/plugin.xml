<idea-plugin version="2">
    <id>com.staremax.vhdlplug</id>
    <name>VHDL Plugin</name>
    <version>1.3.1</version>
    <vendor email="support@staremax.com" url="http://staremax.com">Staremax</vendor>

    <description><![CDATA[
      VHDL syntax highlight and basic refactoring support
      ]]></description>

    <change-notes><![CDATA[
      v1.3.1<br>
      Build Settings are added.<br><br>

      v1.3<br>
      Doxygen comments are added; basic parser recovery; fixed makefile generator;
      package, architecture usages; other bugfixes.<br><br>

      v1.2<br>
      Added Vhdl module. Improved indentation.<br><br>

      v1.1<br>
      Added some annotators, completion, code folding, reference navigation, usages<br><br>

      v1.0<br>
      Basic syntax highlight was added.<br>
      <small>Including color scheme customization</small>
      ]]>
    </change-notes>

    <!-- please see http://confluence.jetbrains.net/display/IDEADEV/Build+Number+Ranges for description -->
    <idea-version since-build="107.105"/>

    <category>Custom Languages</category>

    <!-- please see http://confluence.jetbrains.net/display/IDEADEV/Plugin+Compatibility+with+IntelliJ+Platform+Products
         on how to target different products -->

    <depends>com.intellij.modules.lang</depends>

    <application-components>
        <component>
            <implementation-class>
                com.staremax.vhdlplug.components.VhdlPluginRegistration
            </implementation-class>
        </component>

        <component>
            <interface-class>com.staremax.vhdlplug.settings.VhdlSettings</interface-class>
            <implementation-class>com.staremax.vhdlplug.settings.VhdlSettings</implementation-class>
        </component>
    </application-components>

    <project-components>
        <component>
            <implementation-class>
                com.staremax.vhdlplug.components.VhdlCompilerLoader
            </implementation-class>
        </component>
    </project-components>

    <actions>
        <group id="VhdlPlugin.GeneratorsGroup" description="Generators Menu Group">
            <separator/>
            <add-to-group group-id="BuildMenu" anchor="first"/>
        </group>

        <action id="VhdlPlugin.GenerateMakefile" class="com.staremax.vhdlplug.ide.action.GenerateMakefileAction"
                text="Generate _Makefiles" description="Generating makefiles for local and CI building"
                icon="/icons/makefile.icon.png">
            <add-to-group group-id="VhdlPlugin.GeneratorsGroup" anchor="first"/>
        </action>

        <action id="VhdlPlugin.GenerateDoxyfile" class="com.staremax.vhdlplug.ide.action.GenerateDoxyfileAction"
                text="Generate _Doxyfile" description="Generating doxygen config file"
                icon="/icons/doxyfile.icon.png">
            <add-to-group group-id="VhdlPlugin.GeneratorsGroup" anchor="after"
                          relative-to-action="VhdlPlugin.GenerateMakefile"/>
        </action>
    </actions>

    <extensions defaultExtensionNs="com.intellij">

        <documentationProvider implementation="com.staremax.vhdlplug.documentation.VhdlDocumentationProvider" />

        <applicationConfigurable instance="com.staremax.vhdlplug.settings.ui.VhdlConfigForm"/>

        <fileTypeFactory implementation="com.staremax.vhdlplug.VhdlFileTypeFactory"/>

        <lang.parserDefinition language="VHDL" implementationClass="com.staremax.vhdlplug.parser.VhdlParserDefinition"/>

        <lang.syntaxHighlighterFactory key="VHDL"
                                       implementationClass="com.staremax.vhdlplug.highlighter.VhdlSyntaxHighlighterFactory"/>

        <colorSettingsPage implementation="com.staremax.vhdlplug.highlighter.VhdlColorSettingsPage"/>

        <annotator language="VHDL" implementationClass="com.staremax.vhdlplug.annotator.VhdlAnnotator"/>

        <codeInsight.lineMarkerProvider language="VHDL"
                                        implementationClass="com.staremax.vhdlplug.linemarker.VhdlLineMarkerProvider"/>

        <completion.contributor language="VHDL"
                                implementationClass="com.staremax.vhdlplug.completion.VhdlCompletionContributor"/>

        <lang.refactoringSupport language="VHDL"
                                 implementationClass="com.staremax.vhdlplug.refactoring.VhdlRefactoringSupportProvider"/>

        <psi.referenceContributor language="VHDL"
                                  implementation="com.staremax.vhdlplug.reference.VhdlReferenceContributor"/>

        <gotoSymbolContributor implementation="com.staremax.vhdlplug.navigation.VhdlEntityContributor"/>

        <gotoSymbolContributor implementation="com.staremax.vhdlplug.navigation.VhdlArchitectureContributor"/>

        <lang.psiStructureViewFactory language="VHDL"
                                      implementationClass="com.staremax.vhdlplug.navigation.structure.VhdlStructureViewFactory"/>

        <lang.foldingBuilder language="VHDL" implementationClass="com.staremax.vhdlplug.folding.VhdlFoldingBuilder"/>

        <lang.findUsagesProvider language="VHDL"
                                 implementationClass="com.staremax.vhdlplug.usages.VhdlEntityUsagesProvider"/>

        <findUsagesHandlerFactory implementation="com.staremax.vhdlplug.usages.VhdlFindUsagesHandlerFactory"/>

        <lang.namesValidator language="VHDL"
                             implementationClass="com.staremax.vhdlplug.refactoring.VhdlNamesValidator"/>

        <lang.formatter language="VHDL"
                        implementationClass="com.staremax.vhdlplug.formatter.VhdlFormattingModelBuilder"/>

        <codeStyleSettingsProvider
                implementation="com.staremax.vhdlplug.formatter.codeStyle.VhdlCodeStyleSettingsProvider"/>

        <langCodeStyleSettingsProvider
                implementation="com.staremax.vhdlplug.formatter.codeStyle.VhdlLanguageCodeStyleSettingsProvider"/>

        <lang.commenter language="VHDL" implementationClass="com.staremax.vhdlplug.commenter.VhdlCommenter"/>

        <moduleType id="VHDL_MODULE"
                    implementationClass="com.staremax.vhdlplug.ide.module.VhdlModuleType"
                    classpathProvider="true"/>

        <fileTemplateGroup implementation="com.staremax.vhdlplug.ide.template.VhdlTemplatesFactory"/>
    </extensions>
</idea-plugin>
