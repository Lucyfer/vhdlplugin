package com.staremax.vhdlplug.properties;

import junit.framework.TestCase;

import java.util.HashMap;
import java.util.Map;

public class PropertiesUtilTest extends TestCase {

    public void testSanitizeKey() throws Exception {
        final String input = "a b c $_d\n {e}$f\n}";
        final String output = "a b c \\$_d\n \\{e\\}\\$f\n\\}";

        assertEquals(output, PropertiesUtil.sanitizeKey(input));
    }

    public void testFillPlaceholders() throws Exception {
        final String key1 = "ke${y1}";
        final String value1 = "value1";

        final String key2 = "with";
        final String value2 = "w_i th/out";

        final String input = "some \n string ${with} \n placeholders \n 1 2 3 $%#^$%#^$%#$%";
        final String output = "some \n string w_i th/out \n placeholders \n 1 2 3 $%#^$%#^$%#$%";
        final Map<String, String> map = new HashMap<String, String>();
        map.put(key1, value1);
        map.put(key2, value2);

        final String result = PropertiesUtil.fillPlaceholders(input, map);

        assertEquals(output, result);
    }
}
