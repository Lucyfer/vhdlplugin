package com.staremax.vhdlplug.compiler;

import com.intellij.openapi.compiler.CompilerMessageCategory;
import junit.framework.TestCase;

import java.util.List;

public class VhdlCompilerOutputStreamParserTest extends TestCase {

    private VhdlCompilerOutputStreamParser parser;

    @Override
    protected void setUp() throws Exception {
        parser = new VhdlCompilerOutputStreamParser("");
    }

    @Override
    protected void tearDown() throws Exception {
        parser = null;
    }

    public void testParseStream_empty() {
        final List<?> output = parser.parseStream("\n\n");
        assertNotNull(output);
        System.out.println(output.size());
        for (Object o : output) {
            System.out.println(o);
        }
        assertTrue(output.isEmpty());
    }

    public void testParseStream_error() {
        final String winPath = "D:/Projects/untitled/src/main.vhdl";
        final String unixPath = "/Projects/untitled/src/main.vhdl";
        final int row = 21;
        final int column = 64;
        final String message = "package or configuration keyword expected";
        final String withoutPath = ":" + row + ":" + column + ": " + message + "\n";
        final String winInput = winPath + withoutPath;
        final String unixInput = unixPath + withoutPath;

        final List<VhdlCompilerMessage> winOutput = parser.parseStream(winInput);
        assertEquals(1, winOutput.size());
        assertEquals(message, winOutput.get(0).getMessage());
        assertEquals(CompilerMessageCategory.ERROR, winOutput.get(0).getCategory());
        assertEquals(row, winOutput.get(0).getRow());
        assertEquals(column, winOutput.get(0).getColumn());
        assertEquals("file://" + winPath, winOutput.get(0).getFileName());

        final List<VhdlCompilerMessage> unixOutput = parser.parseStream(unixInput);
        assertEquals(1, unixOutput.size());
        assertEquals(message, unixOutput.get(0).getMessage());
        assertEquals(CompilerMessageCategory.ERROR, unixOutput.get(0).getCategory());
        assertEquals(row, unixOutput.get(0).getRow());
        assertEquals(column, unixOutput.get(0).getColumn());
        assertEquals("file://" + unixPath, unixOutput.get(0).getFileName());
    }

    public void testParseStream_warn() {
        final String winPath = "D:/Projects/untitled/src/main.vhdl";
        final String unixPath = "/Projects/untitled/src/main.vhdl";
        final int row = 2;
        final int column = 64;
        final String message = "package does not require a body";
        final String withoutPath = ":" + row + ":" + column + ":warning: " + message + "\n";
        final String winInput = winPath + withoutPath;
        final String unixInput = unixPath + withoutPath;

        final List<VhdlCompilerMessage> winOutput = parser.parseStream(winInput);
        assertEquals(1, winOutput.size());
        assertEquals(message, winOutput.get(0).getMessage());
        assertEquals(CompilerMessageCategory.WARNING, winOutput.get(0).getCategory());
        assertEquals(row, winOutput.get(0).getRow());
        assertEquals(column, winOutput.get(0).getColumn());
        assertEquals("file://" + winPath, winOutput.get(0).getFileName());

        final List<VhdlCompilerMessage> unixOutput = parser.parseStream(unixInput);
        assertEquals(1, unixOutput.size());
        assertEquals("file://" + unixPath, unixOutput.get(0).getFileName());
    }
}
