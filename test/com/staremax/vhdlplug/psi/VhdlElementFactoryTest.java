package com.staremax.vhdlplug.psi;

import com.intellij.testFramework.PsiTestCase;

public class VhdlElementFactoryTest extends PsiTestCase {

    public void testCreateArchitectureName() throws Exception {
        final String archName = "testEntity";
        final VhdlArchitectureName arch = VhdlElementFactory.createArchitectureName(myProject, archName);

        assertNotNull(arch);
        assertEquals(archName, arch.getText());
    }

    public void testCreateEntityName() throws Exception {
        final String entityName = "testEntity";
        final VhdlEntityName entity = VhdlElementFactory.createEntityName(myProject, entityName);

        assertNotNull(entity);
        assertEquals(entityName, entity.getText());
    }

    public void testCreateEntity() throws Exception {
        final String entityName = "testEntity";
        final VhdlEntityDeclaration entity = VhdlElementFactory.createEntity(myProject, entityName);

        assertNotNull(entity);
        assertEquals(entityName, entity.getEntityName().getText());
    }

    public void testCreateArchitecture() throws Exception {
        final String archName = "testArchitecture";
        final VhdlArchitectureBody architecture = VhdlElementFactory.createArchitecture(myProject, archName);

        assertNotNull(architecture);
        assertEquals(archName, architecture.getArchitectureName().getText());
    }

    public void testCreatePackage() throws Exception{
        final String name = "testPackage";
        final VhdlPackageDeclaration p = VhdlElementFactory.createPackageDeclaration(myProject, name);

        assertNotNull(p);
        assertEquals(name, p.getSimpleName().getText());
        assertNotNull(p.getEndSimpleName());

        VhdlEndSimpleName endName = p.getEndSimpleName();
        assertNotNull(endName);
        assertEquals(name, endName.getText());
    }

    public void testCreateSimpleName() throws Exception {
        final String name = "testSimple_NAME";
        final VhdlSimpleName simpleName = VhdlElementFactory.createSimpleName(myProject, name);

        assertNotNull(simpleName);
        assertEquals(name, simpleName.getText());
    }
}
