package com.staremax.vhdlplug.psi.impl;

import com.intellij.testFramework.PsiTestCase;
import com.staremax.vhdlplug.psi.*;

public class VhdlPsiImplUtilTest extends PsiTestCase {

    public void testSetName_ArchitectureBody() throws Exception {
        final String archName = "testArch";
        final String newArchName = "newArchName";
        final VhdlArchitectureBody arch = VhdlElementFactory.createArchitecture(myProject, archName);

        final VhdlArchitectureBody newArchBody = (VhdlArchitectureBody) VhdlPsiImplUtil.setName(arch, newArchName);

        assertEquals(newArchName, newArchBody.getArchitectureName().getText());
    }

    public void testSetName_ArchitectureName() throws Exception {
        final String archNameString = "testArch";
        final String newArchNameString = "newArchName";
        final VhdlArchitectureBody arch = VhdlElementFactory.createArchitecture(myProject, archNameString);
        final VhdlArchitectureName archName = arch.getArchitectureName();

        final VhdlArchitectureName newArchName = (VhdlArchitectureName) VhdlPsiImplUtil.setName(archName, newArchNameString);

        assertNotNull(newArchName);
        assertEquals(newArchNameString, newArchName.getText());
    }

    public void testSetName_EndArchitectureName() throws Exception {
        final String archNameString = "testArch";
        final String newArchNameString = "newArchName";
        final VhdlArchitectureBody arch = VhdlElementFactory.createArchitecture(myProject, archNameString);
        final VhdlEndArchitectureName archName = arch.getEndArchitectureName();

        assertNotNull(archName);
        final VhdlEndArchitectureName newArchName = (VhdlEndArchitectureName) VhdlPsiImplUtil.setName(archName, newArchNameString);

        assertNotNull(newArchName);
        assertEquals(newArchNameString, newArchName.getText());
    }

    public void testSetName_SimpleName() throws Exception {
        final String simpleNameString = "test_Name_STRING01";
        final String newSimpleNameString = "NewTestName0_2";
        final VhdlPackageDeclaration p = VhdlElementFactory.createPackageDeclaration(myProject, simpleNameString);
        final VhdlSimpleName simpleName = p.getSimpleName();

        final VhdlSimpleName newSimpleName = (VhdlSimpleName) VhdlPsiImplUtil.setName(simpleName, newSimpleNameString);

        assertNotNull(newSimpleName);
        assertEquals(newSimpleNameString, newSimpleName.getText());
        assertEquals(newSimpleNameString, p.getSimpleName().getText());
    }
}
