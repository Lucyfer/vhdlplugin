package com.staremax.vhdlplug.lexer;

import com.staremax.vhdlplug.psi.VhdlTypes;
import junit.framework.TestCase;

import java.io.IOException;
import java.io.Reader;

public class VhdlLexerTest extends TestCase {
    private VhdlLexer lexer;

    protected void setUp() {
        lexer = new VhdlLexer((Reader) null);
    }

    protected void tearDown() {
        lexer = null;
    }

    public void testAdvance() throws IOException {
        CharSequence sequence = "ENTITY \n  \t change_state IS-- test comment\r\n"
                + "END ENTITY change_state;--";
        lexer.reset(sequence, 0, sequence.length(), VhdlLexer.YYINITIAL);

        assertEquals(VhdlTypes.ENTITY, lexer.advance());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());
        assertEquals(VhdlTypes.IDENTIFIER, lexer.advance());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());
        assertEquals(VhdlTypes.IS, lexer.advance());
        assertEquals(VhdlTokenTypesExtension.SINGLE_LINE_COMMENT, lexer.advance());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());
        assertEquals(VhdlTypes.END, lexer.advance());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());
        assertEquals(VhdlTypes.ENTITY, lexer.advance());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());
        assertEquals(VhdlTypes.IDENTIFIER, lexer.advance());
        assertEquals(VhdlTypes.SEMICOLON, lexer.advance());
        assertEquals(VhdlTokenTypesExtension.SINGLE_LINE_COMMENT, lexer.advance());
        assertEquals(null, lexer.advance());
    }

    public void testEmptyStringParsing() throws IOException {
        CharSequence empty = "\"\"";
        lexer.reset(empty, 0, empty.length(), VhdlLexer.YYINITIAL);

        assertEquals(VhdlTypes.STRING_LITERAL, lexer.advance());
        assertEquals(empty, lexer.yytext());
        assertEquals(null, lexer.advance());
    }

    public void testMultilineStringParsing() throws IOException {
        CharSequence string = "\"some \n \r\n content \n with escaped characters \\\" \r\n \f \b \" end \"\"";
        lexer.reset(string, 0, string.length(), VhdlLexer.YYINITIAL);

        assertEquals(VhdlTypes.STRING_LITERAL, lexer.advance());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());
        assertEquals(VhdlTypes.END, lexer.advance());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());
        assertEquals(VhdlTypes.STRING_LITERAL, lexer.advance());
        assertEquals(null, lexer.advance());
    }

    public void testCharParsing() throws IOException {
        CharSequence chr = "'a' '\\'' '\\n'";
        lexer.reset(chr, 0, chr.length(), VhdlLexer.YYINITIAL);

        assertEquals(VhdlTypes.CHAR_LITERAL, lexer.advance());
        assertEquals("'a'", lexer.yytext());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());

        assertEquals(VhdlTypes.CHAR_LITERAL, lexer.advance());
        assertEquals("'\\''", lexer.yytext());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());

        assertEquals(VhdlTypes.CHAR_LITERAL, lexer.advance());
        assertEquals("'\\n'", lexer.yytext());
    }

    public void testDoxygen() throws Exception {
        final CharSequence seq = "sometext \r\t \n with --! doxygen @comment !#$%^&*()_+-0123456789?{}[]\\|/<>.,;=`~'\r\n\n";
        lexer.reset(seq, 0, seq.length(), VhdlLexer.YYINITIAL);

        assertEquals(VhdlTypes.IDENTIFIER, lexer.advance());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());
        assertEquals(VhdlTypes.WITH, lexer.advance());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());
        assertEquals(VhdlTokenTypesExtension.DOXYGEN_COMMENT, lexer.advance());
        assertEquals("--!", lexer.yytext());
        assertEquals(VhdlTokenTypesExtension.DOXYGEN_COMMENT, lexer.advance());
        assertEquals(" doxygen ", lexer.yytext());
        assertEquals(VhdlTokenTypesExtension.DOXYGEN_KEYWORD, lexer.advance());
        assertEquals("@comment", lexer.yytext());
        assertEquals(VhdlTokenTypesExtension.DOXYGEN_COMMENT, lexer.advance());
        assertEquals(" !#$%^&*()_+-0123456789?{}[]\\|/<>.,;=`~'", lexer.yytext());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());
        assertEquals("\r\n", lexer.yytext());
        assertEquals(VhdlTokenTypesExtension.WHITESPACE, lexer.advance());
        assertEquals(null, lexer.advance());
    }
}
