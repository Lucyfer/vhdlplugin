package com.staremax.vhdlplug.usages;

import com.intellij.find.findUsages.FindUsagesHandler;
import com.intellij.find.findUsages.FindUsagesHandlerFactory;
import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.psi.*;
import com.staremax.vhdlplug.usages.handler.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VhdlFindUsagesHandlerFactory extends FindUsagesHandlerFactory {
    @Override
    public boolean canFindUsages(@NotNull PsiElement element) {
        return element instanceof VhdlNamedElement;
    }

    @Nullable
    @Override
    public FindUsagesHandler createFindUsagesHandler(@NotNull PsiElement element, boolean highlight) {
        if (element instanceof VhdlEntityName) {
            return new VhdlEntityNameUsagesHandler((VhdlEntityName) element);
        } else if (element instanceof VhdlEndEntityName) {
            return new VhdlEndEntityNameUsagesHandler((VhdlEndEntityName) element);
        } else if (element instanceof VhdlArchitectureName) {
            return new VhdlArchitectureNameUsagesHandler((VhdlArchitectureName) element);
        } else if (element instanceof VhdlEndArchitectureName) {
            return new VhdlEndArchitectureNameUsagesHandler((VhdlEndArchitectureName) element);
        } else if (element instanceof VhdlEntityDeclaration) {
            VhdlEntityDeclaration parent = (VhdlEntityDeclaration) element;
            return new VhdlEntityUsagesHandler(parent);
        } else if (element instanceof VhdlArchitectureBody) {
            VhdlArchitectureBody parent = (VhdlArchitectureBody) element;
            return new VhdlArchitectureNameUsagesHandler(parent.getArchitectureName());
        } else if (element instanceof VhdlSimpleName || element instanceof VhdlEndSimpleName) {
            return null;
        }
        throw new IllegalArgumentException("Unexpected element type: " + element);
    }
}
