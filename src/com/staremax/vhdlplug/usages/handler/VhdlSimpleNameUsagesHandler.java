package com.staremax.vhdlplug.usages.handler;

import com.intellij.find.findUsages.FindUsagesHandler;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.search.SearchScope;
import com.staremax.vhdlplug.reference.provider.VhdlSimpleNameReferenceProvider;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collection;

public class VhdlSimpleNameUsagesHandler extends FindUsagesHandler {

    public VhdlSimpleNameUsagesHandler(@NotNull PsiElement psiElement) {
        super(psiElement);
    }

    @Override
    public Collection<PsiReference> findReferencesToHighlight(@NotNull PsiElement element, @NotNull SearchScope searchScope) {
        return Arrays.asList(VhdlSimpleNameReferenceProvider.getReferences(element));
    }

    @Override
    protected boolean isSearchForTextOccurencesAvailable(@NotNull PsiElement element, boolean isSingleFile) {
        return isSingleFile;
    }
}
