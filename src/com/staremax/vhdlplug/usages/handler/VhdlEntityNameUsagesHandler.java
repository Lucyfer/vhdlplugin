package com.staremax.vhdlplug.usages.handler;

import com.intellij.find.findUsages.FindUsagesHandler;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.search.SearchScope;
import com.staremax.vhdlplug.psi.VhdlEntityName;
import com.staremax.vhdlplug.reference.impl.VhdlEntityNameReference;
import com.staremax.vhdlplug.utils.VhdlUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

public class VhdlEntityNameUsagesHandler extends FindUsagesHandler {

    public VhdlEntityNameUsagesHandler(@NotNull VhdlEntityName element) {
        super(element);
    }

    @Override
    public Collection<PsiReference> findReferencesToHighlight(@NotNull PsiElement element, @NotNull SearchScope searchScope) {
        Collection<PsiReference> references = new ArrayList<PsiReference>();
        if (element instanceof VhdlEntityName) {
            Collection<VhdlEntityName> entityNames = VhdlUtil.findEntityNameEntries(element.getProject(), element.getText());
            for (VhdlEntityName e : entityNames) {
                references.add(new VhdlEntityNameReference(e));
            }
        }
        return references;
    }

    @Override
    protected boolean isSearchForTextOccurencesAvailable(@NotNull PsiElement element, boolean isSingleFile) {
        return isSingleFile;
    }
}
