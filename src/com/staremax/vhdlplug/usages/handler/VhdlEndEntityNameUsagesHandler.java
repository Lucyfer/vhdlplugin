package com.staremax.vhdlplug.usages.handler;

import com.intellij.find.findUsages.FindUsagesHandler;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.search.SearchScope;
import com.staremax.vhdlplug.psi.VhdlEndEntityName;
import com.staremax.vhdlplug.psi.VhdlEntityDeclaration;
import com.staremax.vhdlplug.reference.impl.VhdlEntityNameReference;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

public class VhdlEndEntityNameUsagesHandler extends FindUsagesHandler {

    public VhdlEndEntityNameUsagesHandler(@NotNull VhdlEndEntityName element) {
        super(element);
    }

    @Override
    public Collection<PsiReference> findReferencesToHighlight(@NotNull PsiElement element, @NotNull SearchScope searchScope) {
        Collection<PsiReference> references = new ArrayList<PsiReference>();
        VhdlEntityDeclaration parent = (VhdlEntityDeclaration) element.getParent();
        references.add(new VhdlEntityNameReference(parent.getEntityName()));
        return references;
    }

    @Override
    protected boolean isSearchForTextOccurencesAvailable(@NotNull PsiElement element, boolean isSingleFile) {
        return isSingleFile;
    }
}
