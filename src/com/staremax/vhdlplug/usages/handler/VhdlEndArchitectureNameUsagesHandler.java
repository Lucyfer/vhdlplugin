package com.staremax.vhdlplug.usages.handler;

import com.intellij.find.findUsages.FindUsagesHandler;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.search.SearchScope;
import com.staremax.vhdlplug.psi.VhdlArchitectureBody;
import com.staremax.vhdlplug.psi.VhdlEndArchitectureName;
import com.staremax.vhdlplug.reference.impl.VhdlArchitectureNameReference;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

public class VhdlEndArchitectureNameUsagesHandler extends FindUsagesHandler {

    public VhdlEndArchitectureNameUsagesHandler(@NotNull VhdlEndArchitectureName psiElement) {
        super(psiElement);
    }

    @Override
    public Collection<PsiReference> findReferencesToHighlight(@NotNull PsiElement element, @NotNull SearchScope searchScope) {
        Collection<PsiReference> references = new ArrayList<PsiReference>();
        VhdlArchitectureBody parent = (VhdlArchitectureBody) element.getParent();
        references.add(new VhdlArchitectureNameReference(parent.getArchitectureName()));
        return references;
    }

    @Override
    protected boolean isSearchForTextOccurencesAvailable(@NotNull PsiElement element, boolean isSingleFile) {
        return isSingleFile;
    }
}
