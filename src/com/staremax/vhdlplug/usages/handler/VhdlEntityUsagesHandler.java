package com.staremax.vhdlplug.usages.handler;

import com.intellij.find.findUsages.FindUsagesHandler;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.search.SearchScope;
import com.staremax.vhdlplug.psi.VhdlEndEntityName;
import com.staremax.vhdlplug.psi.VhdlEntityDeclaration;
import com.staremax.vhdlplug.psi.VhdlEntityName;
import com.staremax.vhdlplug.reference.impl.VhdlEndEntityNameReference;
import com.staremax.vhdlplug.reference.impl.VhdlEntityNameReference;
import com.staremax.vhdlplug.utils.VhdlUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

public class VhdlEntityUsagesHandler extends FindUsagesHandler {

    public VhdlEntityUsagesHandler(@NotNull VhdlEntityDeclaration psiElement) {
        super(psiElement);
    }

    @Override
    public Collection<PsiReference> findReferencesToHighlight(@NotNull PsiElement element, @NotNull SearchScope searchScope) {
        Collection<PsiReference> references = new ArrayList<PsiReference>();
        if (element instanceof VhdlEntityDeclaration) {
            final VhdlEntityDeclaration parent = (VhdlEntityDeclaration) element;
            final String name = parent.getEntityName().getText();

            Collection<VhdlEntityName> entityNames = VhdlUtil.findEntityNameEntries(parent.getProject(), name);
            for (VhdlEntityName e : entityNames) {
                references.add(new VhdlEntityNameReference(e));
            }

            final VhdlEndEntityName endName = parent.getEndEntityName();
            if (endName != null) references.add(new VhdlEndEntityNameReference(endName));
        }
        return references;
    }

    @Override
    protected boolean isSearchForTextOccurencesAvailable(@NotNull PsiElement element, boolean isSingleFile) {
        return isSingleFile;
    }
}
