package com.staremax.vhdlplug.usages.handler;

import com.intellij.find.findUsages.FindUsagesHandler;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.search.SearchScope;
import com.staremax.vhdlplug.psi.VhdlArchitectureBody;
import com.staremax.vhdlplug.psi.VhdlArchitectureName;
import com.staremax.vhdlplug.psi.VhdlEndArchitectureName;
import com.staremax.vhdlplug.reference.impl.VhdlArchitectureNameReference;
import com.staremax.vhdlplug.reference.impl.VhdlEndArchitectureNameReference;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

public class VhdlArchitectureNameUsagesHandler extends FindUsagesHandler {

    public VhdlArchitectureNameUsagesHandler(@NotNull VhdlArchitectureName psiElement) {
        super(psiElement);
    }

    @Override
    public Collection<PsiReference> findReferencesToHighlight(@NotNull PsiElement element, @NotNull SearchScope searchScope) {
        VhdlArchitectureBody parent;
        if (element instanceof VhdlArchitectureName) {
            parent = (VhdlArchitectureBody) element.getParent();
        } else if (element instanceof VhdlArchitectureBody) {
            parent = (VhdlArchitectureBody) element;
        } else {
            throw new UnsupportedOperationException();
        }
        Collection<PsiReference> references = new ArrayList<PsiReference>();
        references.add(new VhdlArchitectureNameReference(parent.getArchitectureName()));

        final VhdlEndArchitectureName endName = parent.getEndArchitectureName();
        if (endName != null) references.add(new VhdlEndArchitectureNameReference(endName));

        return references;
    }

    @Override
    protected boolean isSearchForTextOccurencesAvailable(@NotNull PsiElement element, boolean isSingleFile) {
        return isSingleFile;
    }
}
