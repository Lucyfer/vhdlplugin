package com.staremax.vhdlplug.usages;

import com.intellij.find.impl.HelpID;
import com.intellij.lang.cacheBuilder.DefaultWordsScanner;
import com.intellij.lang.cacheBuilder.WordsScanner;
import com.intellij.lang.findUsages.FindUsagesProvider;
import com.intellij.lexer.FlexAdapter;
import com.intellij.psi.PsiElement;
import com.intellij.psi.tree.TokenSet;
import com.staremax.vhdlplug.lexer.VhdlLexer;
import com.staremax.vhdlplug.lexer.VhdlTokenTypesExtension;
import com.staremax.vhdlplug.psi.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Reader;

public class VhdlEntityUsagesProvider implements FindUsagesProvider {

    private static final DefaultWordsScanner WORDS_SCANNER = new DefaultWordsScanner(
            new FlexAdapter(new VhdlLexer((Reader) null)),
            TokenSet.create(VhdlTypes.IDENTIFIER),
            VhdlTokenTypesExtension.COMMENTS,
            VhdlTokenTypesExtension.STRING_LITERALS);

    @Override
    public boolean canFindUsagesFor(@NotNull PsiElement element) {
        return element instanceof VhdlNamedElement;
    }

    @Nullable
    @Override
    public WordsScanner getWordsScanner() {
        return WORDS_SCANNER;
    }

    @Nullable
    @Override
    public String getHelpId(@NotNull PsiElement element) {
        return HelpID.FIND_OTHER_USAGES;
    }

    @NotNull
    @Override
    public String getType(@NotNull PsiElement element) {
        if (element instanceof VhdlEntityDeclaration) {
            return "Entity";
        } else if (element instanceof VhdlArchitectureBody) {
            return "Architecture";
        } else if (element instanceof VhdlArchitectureName) {
            return "Architecture name";
        } else if (element instanceof VhdlEndArchitectureName) {
            return "End Architecture name";
        } else if (element instanceof VhdlEntityName) {
            return "Entity name";
        } else if (element instanceof VhdlEndEntityName) {
            return "End Entity name";
        } else if (element instanceof VhdlSimpleName) {
            final PsiElement parent = element.getParent();
            if (parent instanceof VhdlPackageDeclaration) return "Package";
            else if (parent instanceof VhdlPackageBody) return "Package Body";
        }
        return "Other";
    }

    @NotNull
    @Override
    public String getDescriptiveName(@NotNull PsiElement element) {
        if (element instanceof VhdlNamedElement) {
            final String name = ((VhdlNamedElement) element).getName();
            return name != null ? name : "";
        }
        return "";
    }

    @NotNull
    @Override
    public String getNodeText(@NotNull PsiElement element, boolean useFullName) {
        return getDescriptiveName(element);
    }
}
