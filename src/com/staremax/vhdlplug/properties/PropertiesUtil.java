package com.staremax.vhdlplug.properties;

import java.util.Map;

public class PropertiesUtil {

    /**
     * replaces ${placeholerKey} to placeholderValue
     *
     * @param propertyContent   origin string
     * @param placeholderValues key-value dictionary of placeholders and values
     * @return result string with all replacements
     */
    public static String fillPlaceholders(String propertyContent, Map<String, String> placeholderValues) {
        String result = propertyContent;

        for (String placeholerKey : placeholderValues.keySet()) {
            result = result.replaceAll("\\$\\{" + sanitizeKey(placeholerKey) + "\\}", placeholderValues.get(placeholerKey));
        }

        return result;
    }

    /**
     * add slashes
     *
     * @param key string
     * @return string with changes
     */
    public static String sanitizeKey(String key) {
        return key.replace("$", "\\$").replace("{", "\\{").replace("}", "\\}");
    }
}
