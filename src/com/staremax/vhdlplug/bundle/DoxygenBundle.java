package com.staremax.vhdlplug.bundle;

import com.staremax.vhdlplug.logger.Logger;
import org.jetbrains.annotations.NonNls;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.Properties;

/**
 * Contain doxygen resources
 */
public class DoxygenBundle {

    private static final Logger LOG = Logger.getLogger(DoxygenBundle.class.getName());
    private static final DoxygenBundle INSTANCE = new DoxygenBundle();
    private static Reference<Properties> ourBundle;

    @NonNls
    protected static final String PATH_TO_BUNDLE = "/messages/Doxygen.xml";

    private DoxygenBundle() {
    }

    public static String message(String key)
            throws IOException {
        return INSTANCE.getBundle().getProperty(key);
    }

    private Properties getBundle() throws IOException {
        Properties bundle = null;
        if (ourBundle != null) {
            bundle = ourBundle.get();
        }
        if (bundle == null) {
            bundle = new Properties();
            InputStream inputStream = null;
            try {
                inputStream = this.getClass().getResourceAsStream(PATH_TO_BUNDLE);
                bundle.loadFromXML(inputStream);
                ourBundle = new SoftReference<Properties>(bundle);
            } catch (IOException e) {
                LOG.warn("Cannot load Doxygen properties file", e);
                throw e;
            } finally {
                if (inputStream != null) inputStream.close();
            }
        }
        return bundle;
    }
}
