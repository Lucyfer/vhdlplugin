package com.staremax.vhdlplug.bundle;

import com.staremax.vhdlplug.logger.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

/**
 * Contain compiler text resources
 */
public class CompilerBundle {

    private static Logger LOG = Logger.getLogger(CompilerBundle.class.getName());
    private static String PATH_TO_BUNDLE = "/messages/Compiler.xml";
    private static CompilerBundle INSTANCE = new CompilerBundle();

    public static String message(String key, Object... placeholders) throws IOException {
        final String content = INSTANCE.getBundle().getProperty(key);
        if (placeholders.length != 0) {
            return MessageFormat.format(content, placeholders);
        }
        return content;
    }

    private Properties getBundle() throws IOException {
        final Properties bundle = new Properties();
        InputStream inputStream = null;
        try {
            inputStream = this.getClass().getResourceAsStream(PATH_TO_BUNDLE);
            bundle.loadFromXML(inputStream);
        } catch (IOException e) {
            LOG.warn("Cannot load Compiler properties file", e);
            throw e;
        } finally {
            if (inputStream != null) inputStream.close();
        }
        return bundle;
    }
}
