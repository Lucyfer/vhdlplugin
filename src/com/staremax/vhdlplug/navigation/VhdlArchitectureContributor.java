package com.staremax.vhdlplug.navigation;

import com.intellij.navigation.ChooseByNameContributor;
import com.intellij.navigation.NavigationItem;
import com.intellij.openapi.project.Project;
import com.staremax.vhdlplug.psi.VhdlArchitectureBody;
import com.staremax.vhdlplug.utils.VhdlUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Navigation by architecture names
 */
public class VhdlArchitectureContributor implements ChooseByNameContributor {
    @NotNull
    @Override
    public NavigationItem[] getItemsByName(String name, String pattern, Project project, boolean includeNonProjectItems) {
        List<VhdlArchitectureBody> arch = new ArrayList<VhdlArchitectureBody>(VhdlUtil.findArchitectures(project, name));
        return arch.toArray(new NavigationItem[arch.size()]);
    }

    @NotNull
    @Override
    public String[] getNames(Project project, boolean b) {
        List<VhdlArchitectureBody> arch = new ArrayList<VhdlArchitectureBody>(VhdlUtil.findArchitectures(project));
        List<String> names = new ArrayList<String>();
        for (VhdlArchitectureBody a : arch) {
            names.add(a.getArchitectureNameString());
        }
        return names.toArray(new String[names.size()]);
    }
}
