package com.staremax.vhdlplug.navigation.presentation;

import com.staremax.vhdlplug.psi.VhdlPackageBody;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VhdlPackageBodyPresentation extends VhdlAbstractPresentation<VhdlPackageBody> {

    public VhdlPackageBodyPresentation(@NotNull VhdlPackageBody myElement) {
        super(myElement);
    }

    @Nullable
    @Override
    public String getPresentableText() {
        return getElement().getSimpleName().getText();
    }
}
