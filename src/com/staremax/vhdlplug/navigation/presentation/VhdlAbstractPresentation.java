package com.staremax.vhdlplug.navigation.presentation;

import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiInvalidElementAccessException;
import com.intellij.psi.PsiNamedElement;
import icons.VhdlIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Element presentation for the Structure view
 * @param <T>
 */
public abstract class VhdlAbstractPresentation<T extends PsiElement> implements ItemPresentation {

    protected final T element;

    public VhdlAbstractPresentation(@NotNull T myElement) {
        this.element = myElement;
    }

    @NotNull
    public T getElement() {
        return element;
    }

    @Nullable
    @Override
    public String getPresentableText() {
        if (element instanceof PsiNamedElement) {
            return ((PsiNamedElement) element).getName();
        }
        return null;
    }

    @Nullable
    @Override
    public String getLocationString() {
        try {
            PsiFile file = element.getContainingFile();
            return file != null ? file.getName() : null;
        } catch (PsiInvalidElementAccessException e) {
            return null;
        }
    }

    @Nullable
    @Override
    public Icon getIcon(boolean b) {
        return VhdlIcons.VhdlIcon;
    }
}
