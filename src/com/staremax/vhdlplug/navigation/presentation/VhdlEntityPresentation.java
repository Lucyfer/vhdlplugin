package com.staremax.vhdlplug.navigation.presentation;

import com.staremax.vhdlplug.psi.VhdlEntityDeclaration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VhdlEntityPresentation extends VhdlAbstractPresentation<VhdlEntityDeclaration> {

    public VhdlEntityPresentation(@NotNull VhdlEntityDeclaration element) {
        super(element);
    }

    @Nullable
    @Override
    public String getPresentableText() {
        return getElement().getEntityNameString();
    }
}
