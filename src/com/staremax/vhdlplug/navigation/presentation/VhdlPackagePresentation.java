package com.staremax.vhdlplug.navigation.presentation;

import com.staremax.vhdlplug.psi.VhdlPackageDeclaration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VhdlPackagePresentation extends VhdlAbstractPresentation<VhdlPackageDeclaration> {

    public VhdlPackagePresentation(@NotNull VhdlPackageDeclaration myElement) {
        super(myElement);
    }

    @Nullable
    @Override
    public String getPresentableText() {
        return getElement().getSimpleName().getText();
    }
}
