package com.staremax.vhdlplug.navigation.presentation;

import com.staremax.vhdlplug.psi.VhdlArchitectureBody;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VhdlArchitecturePresentation extends VhdlAbstractPresentation<VhdlArchitectureBody> {

    public VhdlArchitecturePresentation(@NotNull VhdlArchitectureBody myElement) {
        super(myElement);
    }

    @Nullable
    @Override
    public String getPresentableText() {
        return getElement().getArchitectureNameString();
    }
}
