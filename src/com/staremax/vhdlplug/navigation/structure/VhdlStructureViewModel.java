package com.staremax.vhdlplug.navigation.structure;

import com.intellij.ide.structureView.StructureViewModel;
import com.intellij.ide.structureView.StructureViewModelBase;
import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.util.treeView.smartTree.Sorter;
import com.intellij.psi.PsiFile;
import com.staremax.vhdlplug.VhdlFile;
import org.jetbrains.annotations.NotNull;

public class VhdlStructureViewModel extends StructureViewModelBase implements StructureViewModel.ElementInfoProvider {
    public VhdlStructureViewModel(@NotNull PsiFile psiFile) {
        super(psiFile, new VhdlStructureViewElement(psiFile));
    }

    @NotNull
    @Override
    public Sorter[] getSorters() {
        return new Sorter[] {Sorter.ALPHA_SORTER};
    }

    @Override
    public boolean isAlwaysLeaf(StructureViewTreeElement element) {
        return false;
    }

    @Override
    public boolean isAlwaysShowsPlus(StructureViewTreeElement element) {
        return element.getValue() instanceof VhdlFile;
    }
}
