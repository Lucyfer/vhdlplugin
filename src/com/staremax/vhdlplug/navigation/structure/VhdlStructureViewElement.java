package com.staremax.vhdlplug.navigation.structure;

import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.util.treeView.smartTree.SortableTreeElement;
import com.intellij.ide.util.treeView.smartTree.TreeElement;
import com.intellij.navigation.ItemPresentation;
import com.intellij.navigation.NavigationItem;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiNamedElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.staremax.vhdlplug.VhdlFile;
import com.staremax.vhdlplug.psi.*;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class VhdlStructureViewElement implements StructureViewTreeElement, SortableTreeElement {

    private final PsiElement element;

    public VhdlStructureViewElement(@NotNull PsiElement element) {
        this.element = element;
    }

    @Override
    public Object getValue() {
        return element;
    }

    @Override
    public void navigate(boolean requestFocus) {
        if (element instanceof NavigationItem) {
            ((NavigationItem) element).navigate(requestFocus);
        }
    }

    @Override
    public boolean canNavigate() {
        return element instanceof NavigationItem && ((NavigationItem) element).canNavigate();
    }

    @Override
    public boolean canNavigateToSource() {
        return element instanceof NavigationItem && ((NavigationItem) element).canNavigateToSource();
    }

    @Override
    public String getAlphaSortKey() {
        return element instanceof PsiNamedElement ? ((PsiNamedElement) element).getName() : null;
    }

    @Override
    public ItemPresentation getPresentation() {
        return element instanceof NavigationItem ? ((NavigationItem) element).getPresentation() : null;
    }

    @Override
    public TreeElement[] getChildren() {
        if (element instanceof VhdlFile) {
            List<TreeElement> treeElements = new ArrayList<TreeElement>();
            List<VhdlDesignUnit> designUnits = PsiTreeUtil.getChildrenOfTypeAsList(element, VhdlDesignUnit.class);
            for (VhdlDesignUnit d : designUnits) {
                final VhdlPrimaryUnit primaryUnit = d.getLibraryUnit().getPrimaryUnit();
                final VhdlSecondaryUnit secondaryUnit = d.getLibraryUnit().getSecondaryUnit();

                if (primaryUnit != null) {
                    final VhdlEntityDeclaration entity = primaryUnit.getEntityDeclaration();
                    if (entity != null) treeElements.add(new VhdlStructureViewElement(entity));

                    final VhdlPackageDeclaration packageDecl = primaryUnit.getPackageDeclaration();
                    if (packageDecl != null) treeElements.add(new VhdlStructureViewElement(packageDecl));
                }

                if (secondaryUnit != null) {
                    final VhdlArchitectureBody arch = secondaryUnit.getArchitectureBody();
                    if (arch != null) treeElements.add(new VhdlStructureViewElement(arch));

                    final VhdlPackageBody packageBody = secondaryUnit.getPackageBody();
                    if (packageBody != null) treeElements.add(new VhdlStructureViewElement(packageBody));
                }
            }
            return treeElements.toArray(new TreeElement[treeElements.size()]);
        }
        return EMPTY_ARRAY;
    }
}
