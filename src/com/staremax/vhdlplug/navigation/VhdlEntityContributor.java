package com.staremax.vhdlplug.navigation;

import com.intellij.navigation.ChooseByNameContributor;
import com.intellij.navigation.NavigationItem;
import com.intellij.openapi.project.Project;
import com.staremax.vhdlplug.psi.VhdlEntityDeclaration;
import com.staremax.vhdlplug.utils.VhdlUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Navigation by entity names
 */
public class VhdlEntityContributor implements ChooseByNameContributor {
    @NotNull
    @Override
    public NavigationItem[] getItemsByName(String name, String pattern, Project project, boolean includeNonProjectItems) {
        List<VhdlEntityDeclaration> entities = new ArrayList<VhdlEntityDeclaration>(VhdlUtil.findEntities(project, name));
        return entities.toArray(new NavigationItem[entities.size()]);
    }

    @NotNull
    @Override
    public String[] getNames(Project project, boolean includeNonProjectItems) {
        List<VhdlEntityDeclaration> entities = new ArrayList<VhdlEntityDeclaration>(VhdlUtil.findEntities(project));
        List<String> names = new ArrayList<String>();
        for (VhdlEntityDeclaration e : entities) {
            names.add(e.getEntityNameString());
        }
        return names.toArray(new String[names.size()]);
    }
}
