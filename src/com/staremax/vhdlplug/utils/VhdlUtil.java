package com.staremax.vhdlplug.utils;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.indexing.FileBasedIndex;
import com.staremax.vhdlplug.VhdlFile;
import com.staremax.vhdlplug.VhdlFileType;
import com.staremax.vhdlplug.psi.VhdlArchitectureBody;
import com.staremax.vhdlplug.psi.VhdlEntityDeclaration;
import com.staremax.vhdlplug.psi.VhdlEntityName;
import com.staremax.vhdlplug.psi.VhdlPackageDeclaration;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class VhdlUtil {

    public static Collection<VhdlEntityDeclaration> findEntities(Project project, String key) {
        Collection<VhdlEntityDeclaration> result = new HashSet<VhdlEntityDeclaration>();
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, VhdlFileType.INSTANCE,
                GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            VhdlFile simpleFile = (VhdlFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (simpleFile != null) {
                Collection<VhdlEntityDeclaration> entities = PsiTreeUtil.findChildrenOfType(simpleFile, VhdlEntityDeclaration.class);
                if (entities != null) {
                    for (VhdlEntityDeclaration entity : entities) {
                        if (key.equalsIgnoreCase(entity.getEntityNameString())) {
                            result.add(entity);
                        }
                    }
                }
            }
        }
        return result;
    }

    public static Collection<VhdlEntityDeclaration> findEntities(Project project) {
        Set<VhdlEntityDeclaration> result = new HashSet<VhdlEntityDeclaration>();
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, VhdlFileType.INSTANCE,
                GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            VhdlFile simpleFile = (VhdlFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (simpleFile != null) {
                Collection<VhdlEntityDeclaration> properties = PsiTreeUtil.findChildrenOfType(simpleFile, VhdlEntityDeclaration.class);
                if (properties != null) {
                    result.addAll(properties);
                }
            }
        }
        return result;
    }

    public static Collection<VhdlArchitectureBody> findArchitecturesOfEntity(Project project, String entityName) {
        Set<VhdlArchitectureBody> result = new HashSet<VhdlArchitectureBody>();
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, VhdlFileType.INSTANCE,
                GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            VhdlFile simpleFile = (VhdlFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (simpleFile != null) {
                Collection<VhdlArchitectureBody> archs = PsiTreeUtil.findChildrenOfType(simpleFile, VhdlArchitectureBody.class);
                if (archs != null) {
                    for (VhdlArchitectureBody arch : archs) {
                        if (entityName.equalsIgnoreCase(arch.getEntityNameString())) {
                            result.add(arch);
                        }
                    }
                }
            }
        }
        return result;
    }

    public static Collection<VhdlArchitectureBody> findArchitectures(Project project) {
        Set<VhdlArchitectureBody> result = new HashSet<VhdlArchitectureBody>();
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, VhdlFileType.INSTANCE,
                GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            VhdlFile simpleFile = (VhdlFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (simpleFile != null) {
                Collection<VhdlArchitectureBody> archs = PsiTreeUtil.findChildrenOfType(simpleFile, VhdlArchitectureBody.class);
                if (archs != null) {
                    result.addAll(archs);
                }
            }
        }
        return result;
    }

    public static Collection<VhdlArchitectureBody> findArchitectures(Project project, String archName) {
        Set<VhdlArchitectureBody> result = new HashSet<VhdlArchitectureBody>();
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, VhdlFileType.INSTANCE,
                GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            VhdlFile simpleFile = (VhdlFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (simpleFile != null) {
                Collection<VhdlArchitectureBody> archs = PsiTreeUtil.findChildrenOfType(simpleFile, VhdlArchitectureBody.class);
                for (VhdlArchitectureBody a : archs) {
                    if (a.getArchitectureNameString().equalsIgnoreCase(archName)) {
                        result.add(a);
                    }
                }
            }
        }
        return result;
    }

    public static Collection<VhdlEntityName> findEntityNameEntries(Project project, String entityName) {
        Set<VhdlEntityName> result = new HashSet<VhdlEntityName>();
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, VhdlFileType.INSTANCE,
                GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            VhdlFile simpleFile = (VhdlFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (simpleFile != null) {
                for (VhdlArchitectureBody arch : findArchitecturesOfEntity(project, entityName)) {
                    result.add(arch.getEntityName());
                }
                for (VhdlEntityDeclaration e : findEntities(project, entityName)) {
                    result.add(e.getEntityName());
                }
            }
        }
        return result;
    }

    public static Collection<VhdlEntityName> findEntityNameEntries(Project project) {
        Set<VhdlEntityName> result = new HashSet<VhdlEntityName>();
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, VhdlFileType.INSTANCE,
                GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            VhdlFile simpleFile = (VhdlFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (simpleFile != null) {
                Collection<VhdlArchitectureBody> archs = findArchitectures(project);
                for (VhdlArchitectureBody arch : archs) {
                    result.add(arch.getEntityName());
                }
                for (VhdlEntityDeclaration e : findEntities(project)) {
                    result.add(e.getEntityName());
                }
            }
        }
        return result;
    }

    public static Collection<VhdlPackageDeclaration> findPackages(Project project, String packageName) {
        Set<VhdlPackageDeclaration> result = new HashSet<VhdlPackageDeclaration>();
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, VhdlFileType.INSTANCE,
                GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            VhdlFile simpleFile = (VhdlFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (simpleFile != null) {
                Collection<VhdlPackageDeclaration> pkgs = PsiTreeUtil.findChildrenOfType(simpleFile, VhdlPackageDeclaration.class);
                for (VhdlPackageDeclaration p : pkgs) {
                    if (p.getSimpleName().getText().equalsIgnoreCase(packageName)) {
                        result.add(p);
                    }
                }
            }
        }
        return result;
    }
}
