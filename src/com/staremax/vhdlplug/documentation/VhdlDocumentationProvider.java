package com.staremax.vhdlplug.documentation;

import com.intellij.lang.documentation.AbstractDocumentationProvider;
import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.psi.VhdlArchitectureBody;
import com.staremax.vhdlplug.psi.VhdlEntityDeclaration;

/**
 * Internal documentation. Tooltips on ctrl-hover.
 */
public class VhdlDocumentationProvider extends AbstractDocumentationProvider {

    @Override
    public String getQuickNavigateInfo(PsiElement element, PsiElement originalElement) {
        if (element instanceof VhdlEntityDeclaration) {
            final VhdlEntityDeclaration entity = (VhdlEntityDeclaration) element;
            return String.format("Entity \"%s\" [%s]", entity.getName(), entity.getContainingFile().getName());
        }
        if (element instanceof VhdlArchitectureBody) {
            final VhdlArchitectureBody arch = (VhdlArchitectureBody) element;
            return String.format("Architecture \"%s\" [%s]", arch.getName(), arch.getContainingFile().getName());
        }
        return null;
    }
}
