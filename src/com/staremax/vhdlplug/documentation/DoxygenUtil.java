package com.staremax.vhdlplug.documentation;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.staremax.vhdlplug.bundle.DoxygenBundle;
import com.staremax.vhdlplug.bundle.VhdlBundle;
import com.staremax.vhdlplug.ide.notification.VhdlNotificationUtil;
import com.staremax.vhdlplug.logger.Logger;

import java.io.File;
import java.io.PrintWriter;

/**
 * Generates doxygen config
 */
public class DoxygenUtil {

    private static final Logger LOG = Logger.getLogger(DoxygenUtil.class.getName());
    private static final String DOXYFILE = "Doxyfile";

    public static void generateDoxyfile(Project project) {
        String basePath = project.getBaseDir().getPath();
        File doxyfile = new File(basePath, DOXYFILE);
        PrintWriter writer = null;
        try {
            final String content = DoxygenBundle.message("doxyfile");

            writer = new PrintWriter(doxyfile);
            writer.write(content);

            final String title = VhdlBundle.message("vhdl.notification.title.info");
            final String message = VhdlBundle.message("vhdl.doxyfile.generate.successful");
            VhdlNotificationUtil.busInfoFireNotification(title, message);
        } catch (Exception e) {
            LOG.warn("File Error", e);
            Messages.showErrorDialog(project, e.getMessage(), "File Error");
        } finally {
            if (writer != null) writer.close();
        }
    }
}
