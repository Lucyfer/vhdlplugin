package com.staremax.vhdlplug;

import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.vfs.CharsetToolkit;
import com.intellij.openapi.vfs.VirtualFile;
import com.staremax.vhdlplug.bundle.VhdlBundle;
import icons.VhdlIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.nio.charset.Charset;

public class VhdlFileType extends LanguageFileType {
    public static final LanguageFileType INSTANCE = new VhdlFileType();
    public static final String DEFAULT_EXTENSION = "vhdl";
    public static final String DOT_DEFAULT_EXTENSION = '.' + DEFAULT_EXTENSION;
    public static final String DESCRIPTION = "VHDL Source File";

    private VhdlFileType() {
        super(VhdlLanguage.INSTANCE);
    }

    @Override
    @NotNull
    public String getName() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getDescription() {
        return VhdlBundle.message("vhdl.files.file.type.description");
    }

    @Override
    @NotNull
    public String getDefaultExtension() {
        return DEFAULT_EXTENSION;
    }

    @Override
    public Icon getIcon() {
        return VhdlIcons.VhdlIcon;
    }

    @Override
    public String getCharset(@NotNull VirtualFile file, final byte[] content) {
        Charset charset = CharsetToolkit.getDefaultSystemCharset();
        return charset != null ? charset.name() : "UTF-8";
    }
}
