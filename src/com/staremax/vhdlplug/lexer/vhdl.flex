package com.staremax.vhdlplug.lexer;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import com.staremax.vhdlplug.lexer.VhdlTokenTypesExtension;
import com.staremax.vhdlplug.psi.VhdlTypes;

%%

%public
%class VhdlLexer
%implements FlexLexer
%unicode
%ignorecase
%function advance
%type IElementType
%eof{ return;
%eof}

WHITESPACE=[\ \t\f\r\n]+
NEWLINE=\r\n | \r | \n
PUNCT=[!#\$%\^&*)(\-+\\/\[\]{};:\'\",.<>?|=`~]
DOXYGEN_COMMENT_BEGIN="--!"
DOXYGEN_KEYWORD="@"[:letter:]*
DOXYGEN_CONTENT=([:letter:]|[:digit:]|_|[\ \t\f]|{PUNCT})+
SINGLE_LINE_COMMENT="--" [^!] .* | "--"
IDENTIFIER=[:letter:] ([0-9]|[:letter:]|_)*
BASED_LITERAL=[0-9]+ "#" [0-9]+ ("_" [0-9]+)* "#" ([eE][+-]?[0-9]+)?
DECIMAL_LITERAL=[0-9]+ ("_" [0-9]+)* ("." [0-9]+ ("_" [0-9]+)*)? ([eE][+-]?[0-9]+)?
STRING_LITERAL="\"" ([^\"] | "\\\"")*? "\""
BIT_STRING_LITERAL=[bBoOxX] "\"" [0-9]+ ("_" [0-9]+)* "\""
CHAR_LITERAL="'" ([^'] | \\.) "'"
BAD_CHARACTER=.

%state IN_DOXY_COMMENT

%%

<YYINITIAL> "ABS"           { return VhdlTypes.ABS; }
<YYINITIAL> "ACCESS"        { return VhdlTypes.ACCESS; }
//<YYINITIAL> "ACROSS"        { return VhdlTypes.ACROSS; } // todo
<YYINITIAL> "AFTER"         { return VhdlTypes.AFTER; }
<YYINITIAL> "ALIAS"         { return VhdlTypes.ALIAS; }
<YYINITIAL> "ALL"           { return VhdlTypes.ALL; }
<YYINITIAL> "AND"           { return VhdlTypes.AND; }
<YYINITIAL> "ARCHITECTURE"  { return VhdlTypes.ARCHITECTURE; }
<YYINITIAL> "ARRAY"         { return VhdlTypes.ARRAY; }
<YYINITIAL> "ASSERT"        { return VhdlTypes.ASSERT; }
<YYINITIAL> "ATTRIBUTE"     { return VhdlTypes.ATTRIBUTE; }
<YYINITIAL> "BEGIN"         { return VhdlTypes.BEGIN; }
<YYINITIAL> "BLOCK"         { return VhdlTypes.BLOCK; }
<YYINITIAL> "BODY"          { return VhdlTypes.BODY; }
//<YYINITIAL> "BREAK"         { return VhdlTypes.BREAK; } // todo
<YYINITIAL> "BUFFER"        { return VhdlTypes.BUFFER; }
<YYINITIAL> "BUS"           { return VhdlTypes.BUS; }
<YYINITIAL> "CASE"          { return VhdlTypes.CASE; }
<YYINITIAL> "COMPONENT"     { return VhdlTypes.COMPONENT; }
<YYINITIAL> "CONFIGURATION" { return VhdlTypes.CONFIGURATION; }
<YYINITIAL> "CONSTANT"      { return VhdlTypes.CONSTANT; }
<YYINITIAL> "DISCONNECT"    { return VhdlTypes.DISCONNECT; }
<YYINITIAL> "DOWNTO"        { return VhdlTypes.DOWNTO; }
<YYINITIAL> "ELSE"          { return VhdlTypes.ELSE; }
<YYINITIAL> "ELSIF"         { return VhdlTypes.ELSIF; }
<YYINITIAL> "END"           { return VhdlTypes.END; }
<YYINITIAL> "ENTITY"        { return VhdlTypes.ENTITY; }
<YYINITIAL> "EXIT"          { return VhdlTypes.EXIT; }
<YYINITIAL> "FILE"          { return VhdlTypes.FILE; }
<YYINITIAL> "FOR"           { return VhdlTypes.FOR; }
<YYINITIAL> "FUNCTION"      { return VhdlTypes.FUNCTION; }
<YYINITIAL> "GENERATE"      { return VhdlTypes.GENERATE; }
<YYINITIAL> "GENERIC"       { return VhdlTypes.GENERIC; }
<YYINITIAL> "GROUP"         { return VhdlTypes.GROUP; }
<YYINITIAL> "GUARDED"       { return VhdlTypes.GUARDED; }
<YYINITIAL> "IF"            { return VhdlTypes.IF; }
<YYINITIAL> "IMPURE"        { return VhdlTypes.IMPURE; }
<YYINITIAL> "IN"            { return VhdlTypes.IN; }
<YYINITIAL> "INERTIAL"      { return VhdlTypes.INERTIAL; }
<YYINITIAL> "INOUT"         { return VhdlTypes.INOUT; }
<YYINITIAL> "IS"            { return VhdlTypes.IS; }
<YYINITIAL> "LABEL"         { return VhdlTypes.LABEL; }
<YYINITIAL> "LIBRARY"       { return VhdlTypes.LIBRARY; }
<YYINITIAL> "LINKAGE"       { return VhdlTypes.LINKAGE; }
<YYINITIAL> "LITERAL"       { return VhdlTypes.LITERAL; }
<YYINITIAL> "LOOP"          { return VhdlTypes.LOOP; }
<YYINITIAL> "MAP"           { return VhdlTypes.MAP; }
<YYINITIAL> "MOD"           { return VhdlTypes.MOD; }
<YYINITIAL> "NAND"          { return VhdlTypes.NAND; }
//<YYINITIAL> "NATURE"        { return VhdlTypes.NATURE; }    // todo
<YYINITIAL> "NEW"           { return VhdlTypes.NEW; }
<YYINITIAL> "NEXT"          { return VhdlTypes.NEXT; }
//<YYINITIAL> "NOISE"         { return VhdlTypes.NOISE; }     // todo
<YYINITIAL> "NOR"           { return VhdlTypes.NOR; }
<YYINITIAL> "NOT"           { return VhdlTypes.NOT; }
<YYINITIAL> "NULL"          { return VhdlTypes.NULL; }
<YYINITIAL> "OF"            { return VhdlTypes.OF; }
<YYINITIAL> "ON"            { return VhdlTypes.ON; }
<YYINITIAL> "OPEN"          { return VhdlTypes.OPEN; }
<YYINITIAL> "OR"            { return VhdlTypes.OR; }
<YYINITIAL> "OTHERS"        { return VhdlTypes.OTHERS; }
<YYINITIAL> "OUT"           { return VhdlTypes.OUT; }
<YYINITIAL> "PACKAGE"       { return VhdlTypes.PACKAGE; }
<YYINITIAL> "PORT"          { return VhdlTypes.PORT; }
<YYINITIAL> "POSTPONED"     { return VhdlTypes.POSTPONED; }
//<YYINITIAL> "PROCEDURAL"    { return VhdlTypes.PROCEDURAL; }    // todo
<YYINITIAL> "PROCEDURE"     { return VhdlTypes.PROCEDURE; }
<YYINITIAL> "PROCESS"       { return VhdlTypes.PROCESS; }
<YYINITIAL> "PURE"          { return VhdlTypes.PURE; }
//<YYINITIAL> "QUANTITY"      { return VhdlTypes.QUANTITY; }      // todo
<YYINITIAL> "RANGE"         { return VhdlTypes.RANGE; }
<YYINITIAL> "RECORD"        { return VhdlTypes.RECORD; }
<YYINITIAL> "REGISTER"      { return VhdlTypes.REGISTER; }
<YYINITIAL> "REJECT"        { return VhdlTypes.REJECT; }
<YYINITIAL> "REM"           { return VhdlTypes.REM; }
<YYINITIAL> "REPORT"        { return VhdlTypes.REPORT; }
<YYINITIAL> "RETURN"        { return VhdlTypes.RETURN; }
<YYINITIAL> "ROL"           { return VhdlTypes.ROL; }
<YYINITIAL> "ROR"           { return VhdlTypes.ROR; }
<YYINITIAL> "SELECT"        { return VhdlTypes.SELECT; }
<YYINITIAL> "SEVERITY"      { return VhdlTypes.SEVERITY; }
<YYINITIAL> "SIGNAL"        { return VhdlTypes.SIGNAL; }
<YYINITIAL> "SHARED"        { return VhdlTypes.SHARED; }
<YYINITIAL> "SLA"           { return VhdlTypes.SLA; }
<YYINITIAL> "SLL"           { return VhdlTypes.SLL; }
//<YYINITIAL> "SPECTRUM"      { return VhdlTypes.SPECTRUM; }  // todo
<YYINITIAL> "SRA"           { return VhdlTypes.SRA; }
<YYINITIAL> "SRL"           { return VhdlTypes.SRL; }
//<YYINITIAL> "SUBNATURE"     { return VhdlTypes.SUBNATURE; } // todo
<YYINITIAL> "SUBTYPE"       { return VhdlTypes.SUBTYPE; }
//<YYINITIAL> "TERMINAL"      { return VhdlTypes.TERMINAL; }  // todo
<YYINITIAL> "THEN"          { return VhdlTypes.THEN; }
//<YYINITIAL> "THROUGH"       { return VhdlTypes.THROUGH; }   // todo
<YYINITIAL> "TO"            { return VhdlTypes.TO; }
//<YYINITIAL> "TOLERANCE"     { return VhdlTypes.TOLERANCE; } // todo
<YYINITIAL> "TRANSPORT"     { return VhdlTypes.TRANSPORT; }
<YYINITIAL> "TYPE"          { return VhdlTypes.TYPE; }
<YYINITIAL> "UNAFFECTED"    { return VhdlTypes.UNAFFECTED; }
<YYINITIAL> "UNITS"         { return VhdlTypes.UNITS; }
<YYINITIAL> "UNTIL"         { return VhdlTypes.UNTIL; }
<YYINITIAL> "USE"           { return VhdlTypes.USE; }
<YYINITIAL> "VARIABLE"      { return VhdlTypes.VARIABLE; }
<YYINITIAL> "WAIT"          { return VhdlTypes.WAIT; }
<YYINITIAL> "WHEN"          { return VhdlTypes.WHEN; }
<YYINITIAL> "WHILE"         { return VhdlTypes.WHILE; }
<YYINITIAL> "WITH"          { return VhdlTypes.WITH; }
<YYINITIAL> "XNOR"          { return VhdlTypes.XNOR; }
<YYINITIAL> "XOR"           { return VhdlTypes.XOR; }

<YYINITIAL> {
  {WHITESPACE} { return VhdlTokenTypesExtension.WHITESPACE; }
  {DOXYGEN_COMMENT_BEGIN} { yybegin(IN_DOXY_COMMENT); return VhdlTokenTypesExtension.DOXYGEN_COMMENT; }
  {SINGLE_LINE_COMMENT} { return VhdlTokenTypesExtension.SINGLE_LINE_COMMENT; }
  {STRING_LITERAL} { return VhdlTypes.STRING_LITERAL; }
  {BIT_STRING_LITERAL} { return VhdlTypes.BIT_STRING_LITERAL; }
  {CHAR_LITERAL} { return VhdlTypes.CHAR_LITERAL; }

  {IDENTIFIER} { return VhdlTypes.IDENTIFIER; }

  {BASED_LITERAL} { return VhdlTypes.BASED_LITERAL; }
  {DECIMAL_LITERAL} { return VhdlTypes.DECIMAL_LITERAL; }

  "<>"  { return VhdlTypes.DIAMOND; }
  "=>"  { return VhdlTypes.RARROW; }
  ":="  { return VhdlTypes.ASSIGN; }
  "/="  { return VhdlTypes.NEQ; }
  "<="  { return VhdlTypes.LTEQ; }
  ">="  { return VhdlTypes.GTEQ; }
  "**"  { return VhdlTypes.POWER; }
  "*"   { return VhdlTypes.MULT; }
  "/"   { return VhdlTypes.DIV; }
  "+"   { return VhdlTypes.PLUS; }
  "-"   { return VhdlTypes.MINUS; }
  "&"   { return VhdlTypes.AMP; }
  ","   { return VhdlTypes.COMMA; }
  "("   { return VhdlTypes.LPAREN; }
  ")"   { return VhdlTypes.RPAREN; }
  ":"   { return VhdlTypes.COLON; }
  ";"   { return VhdlTypes.SEMICOLON; }
  "."   { return VhdlTypes.DOT; }
  "="   { return VhdlTypes.EQ; }
  "<"   { return VhdlTypes.LT; }
  ">"   { return VhdlTypes.GT; }
  "|"   { return VhdlTypes.DELIMITER; }
  "'"   { return VhdlTypes.SINGLE_QUOTE; }
}

<IN_DOXY_COMMENT> {NEWLINE}         { yybegin(YYINITIAL); return VhdlTokenTypesExtension.WHITESPACE; }
<IN_DOXY_COMMENT> {DOXYGEN_KEYWORD} { yybegin(IN_DOXY_COMMENT); return VhdlTokenTypesExtension.DOXYGEN_KEYWORD; }
<IN_DOXY_COMMENT> {DOXYGEN_CONTENT} { yybegin(IN_DOXY_COMMENT); return VhdlTokenTypesExtension.DOXYGEN_COMMENT; }
<IN_DOXY_COMMENT> {BAD_CHARACTER}   { yybegin(IN_DOXY_COMMENT); return VhdlTokenTypesExtension.BAD_CHARACTER; }

<YYINITIAL> {BAD_CHARACTER} { return VhdlTokenTypesExtension.BAD_CHARACTER; }
