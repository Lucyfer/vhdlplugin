package com.staremax.vhdlplug.lexer;

import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.tree.TokenSet;
import com.staremax.vhdlplug.psi.VhdlTypes;

public interface VhdlTokenTypesExtension {
    IElementType BAD_CHARACTER = TokenType.BAD_CHARACTER;
    IElementType WHITESPACE = TokenType.WHITE_SPACE;
    IElementType SINGLE_LINE_COMMENT = new VhdlTokenType("SINGLE_LINE_COMMENT");
    IElementType DOXYGEN_COMMENT = new VhdlTokenType("DOXYGEN_COMMENT");
    IElementType DOXYGEN_KEYWORD = new VhdlTokenType("DOXYGEN_KEYWORD");

    TokenSet WHITESPACES = TokenSet.create(WHITESPACE);
    TokenSet COMMENTS = TokenSet.create(SINGLE_LINE_COMMENT, DOXYGEN_COMMENT, DOXYGEN_KEYWORD);
    TokenSet STRING_LITERALS = TokenSet.create(VhdlTypes.STRING_LITERAL, VhdlTypes.BIT_STRING_LITERAL);
}
