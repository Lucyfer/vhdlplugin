package com.staremax.vhdlplug.lexer;

import com.intellij.psi.tree.IElementType;
import com.staremax.vhdlplug.VhdlLanguage;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

/**
 * Defines a token type.
 */
public class VhdlTokenType extends IElementType {
    public VhdlTokenType(@NotNull @NonNls String debugName) {
        super(debugName, VhdlLanguage.INSTANCE);
    }

    @Override
    public String toString() {
        return "VhdlTokenType." + super.toString();
    }
}
