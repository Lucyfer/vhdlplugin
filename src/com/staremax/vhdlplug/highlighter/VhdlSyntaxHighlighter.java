package com.staremax.vhdlplug.highlighter;

import com.intellij.lexer.FlexAdapter;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.tree.IElementType;
import com.staremax.vhdlplug.lexer.VhdlLexer;
import com.staremax.vhdlplug.lexer.VhdlTokenTypesExtension;
import com.staremax.vhdlplug.psi.VhdlTypes;
import org.jetbrains.annotations.NotNull;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

/**
 * Defines highlight types for token.
 */
public class VhdlSyntaxHighlighter extends SyntaxHighlighterBase {
    private static final Map<IElementType, TextAttributesKey[]> colorMapping;

    public static final TextAttributesKey BAD_CHAR;
    public static final TextAttributesKey OPERATOR;
    public static final TextAttributesKey KEYWORD;
    public static final TextAttributesKey STRING;
    public static final TextAttributesKey CHAR;
    public static final TextAttributesKey COMMENT;
    public static final TextAttributesKey IDENTIFIER;
    public static final TextAttributesKey NUMBER;
    public static final TextAttributesKey BRACES;
    private static final TextAttributesKey COMMA;
    private static final TextAttributesKey DOXYGEN_COMMENT;
    private static final TextAttributesKey DOXYGEN_KEYWORD;

    private static final TextAttributesKey[] BAD_CHAR_KEYS;
    private static final TextAttributesKey[] OPERATOR_KEYS;
    private static final TextAttributesKey[] KEYWORD_KEYS;
    private static final TextAttributesKey[] STRING_KEYS;
    private static final TextAttributesKey[] CHAR_KEYS;
    private static final TextAttributesKey[] COMMENT_KEYS;
    private static final TextAttributesKey[] IDENTIFIER_KEYS;
    private static final TextAttributesKey[] NUMBER_KEYS;
    private static final TextAttributesKey[] BRACES_KEYS;
    private static final TextAttributesKey[] COMMA_KEYS;
    private static final TextAttributesKey[] DOXYGEN_COMMENT_KEYS;
    private static final TextAttributesKey[] DOXYGEN_KEYWORD_KEYS;

    private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];

    static {
        colorMapping = new HashMap<IElementType, TextAttributesKey[]>(32);

        BAD_CHAR = createTextAttributesKey("VHDL_BAD_CHARACTER", DefaultLanguageHighlighterColors.INVALID_STRING_ESCAPE);
        OPERATOR = createTextAttributesKey("VHDL_OPERATOR", DefaultLanguageHighlighterColors.OPERATION_SIGN);
        KEYWORD = createTextAttributesKey("VHDL_KEYWORD", DefaultLanguageHighlighterColors.KEYWORD);
        STRING = createTextAttributesKey("VHDL_STRING_LITERAL", DefaultLanguageHighlighterColors.STRING);
        CHAR = createTextAttributesKey("VHDL_CHAR_LITERAL", DefaultLanguageHighlighterColors.STRING);
        COMMENT = createTextAttributesKey("VHDL_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
        IDENTIFIER = createTextAttributesKey("VHDL_IDENTIFIER", DefaultLanguageHighlighterColors.IDENTIFIER);
        NUMBER = createTextAttributesKey("VHDL_NUMBER", DefaultLanguageHighlighterColors.NUMBER);
        BRACES = createTextAttributesKey("VHDL_BRACE", DefaultLanguageHighlighterColors.BRACES);
        COMMA = createTextAttributesKey("VHDL_COMMA", DefaultLanguageHighlighterColors.COMMA);
        DOXYGEN_COMMENT = createTextAttributesKey("DOXY_COMMENT", DefaultLanguageHighlighterColors.DOC_COMMENT);
        DOXYGEN_KEYWORD = createTextAttributesKey("DOXY_KEYWORD", DefaultLanguageHighlighterColors.DOC_COMMENT_TAG_VALUE);

        BAD_CHAR_KEYS = new TextAttributesKey[]{BAD_CHAR};
        OPERATOR_KEYS = new TextAttributesKey[]{OPERATOR};
        KEYWORD_KEYS = new TextAttributesKey[]{KEYWORD};
        STRING_KEYS = new TextAttributesKey[]{STRING};
        CHAR_KEYS = new TextAttributesKey[]{CHAR};
        COMMENT_KEYS = new TextAttributesKey[]{COMMENT};
        IDENTIFIER_KEYS = new TextAttributesKey[]{IDENTIFIER};
        NUMBER_KEYS = new TextAttributesKey[]{NUMBER};
        BRACES_KEYS = new TextAttributesKey[]{BRACES};
        COMMA_KEYS = new TextAttributesKey[]{COMMA};
        DOXYGEN_COMMENT_KEYS = new TextAttributesKey[]{DOXYGEN_COMMENT};
        DOXYGEN_KEYWORD_KEYS = new TextAttributesKey[]{DOXYGEN_KEYWORD};

        colorMapping.put(VhdlTokenTypesExtension.BAD_CHARACTER, BAD_CHAR_KEYS);
        colorMapping.put(VhdlTypes.IDENTIFIER, IDENTIFIER_KEYS);
        colorMapping.put(VhdlTokenTypesExtension.SINGLE_LINE_COMMENT, COMMENT_KEYS);
        colorMapping.put(VhdlTypes.STRING_LITERAL, STRING_KEYS);
        colorMapping.put(VhdlTypes.BIT_STRING_LITERAL, STRING_KEYS);
        colorMapping.put(VhdlTypes.CHAR_LITERAL, CHAR_KEYS);

        colorMapping.put(VhdlTokenTypesExtension.DOXYGEN_COMMENT, DOXYGEN_COMMENT_KEYS);
        colorMapping.put(VhdlTokenTypesExtension.DOXYGEN_KEYWORD, DOXYGEN_KEYWORD_KEYS);

        colorMapping.put(VhdlTypes.BASED_LITERAL, NUMBER_KEYS);
        colorMapping.put(VhdlTypes.DECIMAL_LITERAL, NUMBER_KEYS);

        colorMapping.put(VhdlTypes.LPAREN, BRACES_KEYS);
        colorMapping.put(VhdlTypes.RPAREN, BRACES_KEYS);
        colorMapping.put(VhdlTypes.COMMA, COMMA_KEYS);

        colorMapping.put(VhdlTypes.ABS, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ACCESS, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.AFTER, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ALIAS, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ALL, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.AND, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ARCHITECTURE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ARRAY, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ASSERT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ASSIGN, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ATTRIBUTE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.BEGIN, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.BLOCK, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.BODY, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.BUFFER, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.BUS, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.CASE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.COMPONENT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.CONFIGURATION, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.CONSTANT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.DISCONNECT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.DIV, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.DOWNTO, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ELSE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ELSIF, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.END, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ENTITY, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.EXIT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.FILE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.FOR, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.FUNCTION, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.GENERATE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.GENERIC, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.GROUP, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.GUARDED, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.IF, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.IMPURE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.IN, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.INERTIAL, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.INOUT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.IS, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.LABEL, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.LIBRARY, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.LINKAGE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.LITERAL, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.LOOP, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.MAP, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.MOD, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.NAND, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.NEW, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.NEXT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.NOR, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.NOT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.NULL, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.OF, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ON, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.OPEN, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.OR, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.OTHERS, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.OUT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.PACKAGE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.PORT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.POSTPONED, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.PROCEDURE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.PROCESS, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.PURE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.RANGE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.RECORD, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.REGISTER, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.REJECT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.REM, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.REPORT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.RETURN, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ROL, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.ROR, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.SELECT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.SEVERITY, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.SHARED, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.SIGNAL, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.SLA, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.SLL, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.SRA, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.SRL, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.SUBTYPE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.THEN, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.TO, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.TRANSPORT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.TYPE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.UNAFFECTED, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.UNITS, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.UNTIL, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.USE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.VARIABLE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.WAIT, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.WHEN, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.WHILE, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.WITH, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.XNOR, KEYWORD_KEYS);
        colorMapping.put(VhdlTypes.XOR, KEYWORD_KEYS);

        colorMapping.put(VhdlTypes.DIAMOND, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.RARROW, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.ASSIGN, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.NEQ, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.LTEQ, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.GTEQ, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.POWER, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.MULT, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.DIV, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.PLUS, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.MINUS, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.AMP, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.COLON, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.SEMICOLON, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.DOT, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.EQ, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.LT, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.GT, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.DELIMITER, OPERATOR_KEYS);
        colorMapping.put(VhdlTypes.SINGLE_QUOTE, OPERATOR_KEYS);
    }

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new FlexAdapter(new VhdlLexer((Reader) null));
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType token) {
        if (colorMapping.containsKey(token))
            return colorMapping.get(token);
        return EMPTY_KEYS;
    }
}
