package com.staremax.vhdlplug.highlighter;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import com.staremax.vhdlplug.VhdlLanguage;
import icons.VhdlIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Map;

/**
 * Highlight settings
 */
public class VhdlColorSettingsPage implements ColorSettingsPage {
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
            new AttributesDescriptor("Operator", VhdlSyntaxHighlighter.OPERATOR),
            new AttributesDescriptor("Keyword", VhdlSyntaxHighlighter.KEYWORD),
            new AttributesDescriptor("String", VhdlSyntaxHighlighter.STRING),
            new AttributesDescriptor("Character", VhdlSyntaxHighlighter.CHAR),
            new AttributesDescriptor("Comment", VhdlSyntaxHighlighter.COMMENT),
            new AttributesDescriptor("Identifier", VhdlSyntaxHighlighter.IDENTIFIER),
            new AttributesDescriptor("Number", VhdlSyntaxHighlighter.NUMBER),
    };

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return VhdlIcons.VhdlIcon;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new VhdlSyntaxHighlighter();
    }

    @NotNull
    @Override
    public String getDemoText() {
        return
        "-- comment line\n" +
        "TYPE bit IS ('0', '1');\n" +
        "\n" +
        "--! @brief doxygen comment\n" +
        "ENTITY change_state IS\n" +
        "   PORT(SIGNAL x: IN  real;\n" +
        "        SIGNAL y: OUT real);\n" +
        "END ENTITY change_state;\n" +
        "\n" +
        "ARCHITECTURE behavior OF change_state IS\n" +
        "   SIGNAL bitArrayName : bit_vector (3 downto 0):=\"0000\";\n" +
        "BEGIN\n" +
        "   p: PROCESS(x)\n" +
        "   BEGIN\n" +
        "       IF x > 5.0 THEN y <= 0.1;\n" +
        "       ELSE IF x < 0 THEN y <= -5.0;\n" +
        "       END IF; END IF;\n" +
        "   END PROCESS p;\n" +
        "   bitArrayName <= \"1111\";\n"+
        "END ARCHITECTURE behavior;";
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return VhdlLanguage.LANGUAGE_ID;
    }
}
