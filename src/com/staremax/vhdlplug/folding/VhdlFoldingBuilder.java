package com.staremax.vhdlplug.folding;

import com.intellij.lang.ASTNode;
import com.intellij.lang.folding.FoldingBuilderEx;
import com.intellij.lang.folding.FoldingDescriptor;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.FoldingGroup;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.staremax.vhdlplug.psi.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Code folding
 */
public class VhdlFoldingBuilder extends FoldingBuilderEx {
    @NotNull
    @Override
    public FoldingDescriptor[] buildFoldRegions(@NotNull PsiElement root, @NotNull Document document, boolean quick) {
        List<FoldingDescriptor> descriptors = new ArrayList<FoldingDescriptor>();
        FoldingGroup importsGroup = FoldingGroup.newGroup("Imports");

        Collection<VhdlPackageDeclaration> packages = PsiTreeUtil.findChildrenOfType(root, VhdlPackageDeclaration.class);
        for (final VhdlPackageDeclaration p : packages) {
            descriptors.add(new FoldingDescriptor(p.getNode(), p.getTextRange()) {
                @Nullable
                @Override
                public String getPlaceholderText() {
                    return "Package " + p.getSimpleName().getText();
                }
            });
        }

        Collection<VhdlPackageBody> packageBodies = PsiTreeUtil.findChildrenOfType(root, VhdlPackageBody.class);
        for (final VhdlPackageBody p : packageBodies) {
            descriptors.add(new FoldingDescriptor(p.getNode(), p.getTextRange()) {
                @Nullable
                @Override
                public String getPlaceholderText() {
                    return "Package body " + p.getSimpleName().getText();
                }
            });
        }

        Collection<VhdlEntityDeclaration> entities = PsiTreeUtil.findChildrenOfType(root, VhdlEntityDeclaration.class);
        for (final VhdlEntityDeclaration e : entities) {
            descriptors.add(new FoldingDescriptor(e.getNode(), e.getTextRange()) {
                @Nullable
                @Override
                public String getPlaceholderText() {
                    return "Entity " + e.getEntityNameString();
                }
            });
        }

        Collection<VhdlArchitectureBody> architectures = PsiTreeUtil.findChildrenOfType(root, VhdlArchitectureBody.class);
        for (final VhdlArchitectureBody a : architectures) {
            descriptors.add(new FoldingDescriptor(a.getNode(), a.getTextRange()) {
                @Nullable
                @Override
                public String getPlaceholderText() {
                    return "Architecture " + a.getArchitectureNameString();
                }
            });
        }

        Collection<VhdlContextItem> contextItems = PsiTreeUtil.findChildrenOfType(root, VhdlContextItem.class);
        for (final VhdlContextItem item : contextItems) {
            descriptors.add(new FoldingDescriptor(item.getNode(), item.getTextRange(), importsGroup) {
                @Nullable
                @Override
                public String getPlaceholderText() {
                    if (item.getLibraryClause() != null) {
                        return "Library clause";
                    }
                    if (item.getUseClause() != null) {
                        return "Use clause";
                    }
                    return "Imports";
                }
            });
        }

        return descriptors.toArray(new FoldingDescriptor[descriptors.size()]);
    }

    @Override
    public boolean isCollapsedByDefault(@NotNull ASTNode astNode) {
        return false;
    }

    @Nullable
    @Override
    public String getPlaceholderText(@NotNull ASTNode astNode) {
        return "...";
    }
}
