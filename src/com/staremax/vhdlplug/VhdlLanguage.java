package com.staremax.vhdlplug;

import com.intellij.lang.Language;
import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.Nullable;

public class VhdlLanguage extends Language {
    public static final String LANGUAGE_ID = "VHDL";
    public static final String MIME_TYPE = "text/x-vhdl";
    public static final VhdlLanguage INSTANCE = new VhdlLanguage();

    private VhdlLanguage() {
        super(LANGUAGE_ID, MIME_TYPE);
    }

    @Nullable
    @Override
    public LanguageFileType getAssociatedFileType() {
        return VhdlFileType.INSTANCE;
    }
}
