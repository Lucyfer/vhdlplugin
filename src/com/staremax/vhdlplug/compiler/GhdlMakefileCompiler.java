package com.staremax.vhdlplug.compiler;

import com.intellij.execution.configurations.GeneralCommandLine;
import com.intellij.openapi.compiler.CompileContext;
import com.intellij.openapi.compiler.CompileScope;
import com.intellij.openapi.compiler.CompilerMessageCategory;
import com.intellij.openapi.compiler.TranslatingCompiler;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.Chunk;
import com.staremax.vhdlplug.VhdlFileType;
import com.staremax.vhdlplug.logger.Logger;
import com.staremax.vhdlplug.settings.VhdlSettings;
import com.staremax.vhdlplug.settings.VhdlState;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashMap;

/**
 * Uses a Makefile for compiling *.vhdl sources
 */
public class GhdlMakefileCompiler implements TranslatingCompiler {

    private static final Logger LOG = Logger.getLogger(GhdlMakefileCompiler.class.getName());
    public static final String MAKEFILE_LOCAL = "Makefile-local";
    public static final String MAKEFILE_LOCAL_SRC = "Makefile-local-src";

    final Project project;

    public GhdlMakefileCompiler(Project project) {
        this.project = project;
    }

    @NotNull
    public String getDescription() {
        return "Vhdl Makefile Compiler";
    }

    public boolean validateConfiguration(CompileScope scope) {
        return true;
    }

    public boolean isCompilableFile(VirtualFile file, CompileContext context) {
        return file.getFileType().equals(VhdlFileType.INSTANCE);
    }

    public void compile(CompileContext context, Chunk<Module> moduleChunk, VirtualFile[] files, OutputSink sink) {
        String basePath = project.getBaseDir().getPath();
        File makeFile = new File(basePath, MAKEFILE_LOCAL);
        if (!makeFile.exists()) {
            CompilerUtil.generateLocalMakefile(project, MAKEFILE_LOCAL, MAKEFILE_LOCAL_SRC);
            String message = "Makefile doesn't exist at " + makeFile.getPath() + ". Generating...";
            context.addMessage(CompilerMessageCategory.INFORMATION, message, null, -1, -1);
        }

        CompilerUtil.generateSourcesForMakefile(project, files, MAKEFILE_LOCAL_SRC);

        GeneralCommandLine command = new GeneralCommandLine();
        command.setExePath(getMakeToolBinary());
        command.addParameter("-C");
        command.addParameter(project.getBaseDir().getPath());
        command.addParameter("-f");
        command.addParameter(makeFile.getPath());
        command.addParameter("-e");
        command.addParameter("-s");
        command.addParameter("all");
        command.setEnvParams(new HashMap<String, String>() {{
            //put("SOME_ENV", "some-value");
        }});

        CompilationTaskWorker compilationTaskWorker =
                new CompilationTaskWorker(new VhdlCompilerOutputStreamParser(basePath));
        compilationTaskWorker.executeTask(command, basePath, context);
    }

    private String getMakeToolBinary() {
        final VhdlState state = ServiceManager.getService(VhdlSettings.class).getState();
        return (state != null ? state : VhdlSettings.getDefaults()).getMakeToolBinary();
    }
}
