package com.staremax.vhdlplug.compiler;

import com.intellij.openapi.compiler.CompilerMessageCategory;

/**
 * JavaBean class representing compiler message.
 */
public class VhdlCompilerMessage {

    private final CompilerMessageCategory category;
    private final String message;
    private final String fileName;
    private final int row;
    private final int column;

    protected VhdlCompilerMessage(CompilerMessageCategory category, String message,
                                  String fileName, int row, int column) {
        this.category = category;
        this.message = message;
        this.fileName = fileName;
        this.row = row;
        this.column = column;
    }

    public CompilerMessageCategory getCategory() {
        return category;
    }

    public String getMessage() {
        return message;
    }

    public String getFileName() {
        return fileName;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("VhdlCompilerMessage");
        sb.append("{category=").append(category);
        sb.append(", message='").append(message).append("'");
        sb.append(", fileName='").append(fileName).append("'");
        sb.append(", row=").append(row);
        sb.append(", column=").append(column);
        sb.append('}');
        return sb.toString();
    }
}
