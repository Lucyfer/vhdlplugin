package com.staremax.vhdlplug.compiler;

import com.intellij.execution.configurations.GeneralCommandLine;
import com.intellij.openapi.compiler.CompileContext;
import com.intellij.openapi.compiler.CompileScope;
import com.intellij.openapi.compiler.TranslatingCompiler;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.Chunk;
import com.staremax.vhdlplug.VhdlFileType;
import com.staremax.vhdlplug.logger.Logger;
import com.staremax.vhdlplug.settings.VhdlSettings;
import com.staremax.vhdlplug.settings.VhdlState;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Uses GHDL toolkit for compiling *.vhdl sources
 */
public class GhdlCompiler implements TranslatingCompiler {

    private static final Logger LOG = Logger.getLogger(GhdlCompiler.class.getName());

    public static final String COMPILER_DESC = "Compile with Ghdl Toolkit";

    protected final Project project;

    public GhdlCompiler(Project project) {
        this.project = project;
    }

    /**
     * Compiles the specified files.
     *
     * @param context     the context for the current compile operation.
     * @param moduleChunk contains modules that form a cycle. If project module graph has no cycles, a chunk corresponds to a single module
     * @param files       the source files to compile that correspond to the module chunk
     * @param sink        storage that accepts compiler output results
     */
    @Override
    public void compile(CompileContext context, Chunk<Module> moduleChunk, VirtualFile[] files, OutputSink sink) {
        if (LOG.isDebugEnabled()) {
            for (VirtualFile f : files) {
                LOG.debug(f.getCanonicalPath());
            }
        }

        final String baseOutputPath = project.getBasePath() + VfsUtil.VFS_PATH_SEPARATOR + "out";
        final String executionPath = project.getBasePath();

        for (VirtualFile file : files) {
            compileFile(context, file, baseOutputPath, executionPath, sink);
        }
    }

    protected void compileFile(CompileContext context, VirtualFile source, String baseOutputPath,
                               String executionPath, OutputSink sink) {
        final File targetPath = new File(baseOutputPath);
        final String sourcePath = source.getPath();

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("[building] source [%s] into [%s]", sourcePath, baseOutputPath));
        }

        if (!targetPath.exists() && !targetPath.mkdirs()) {
            LOG.warn("Could not create parent dirs: " + targetPath);
        }

        GeneralCommandLine command = new GeneralCommandLine();
        command.setExePath(getGhdlToolBinary());
        command.addParameter("-a");
        command.addParameter("--std=02");
        command.addParameter("--warn-binding");
        command.addParameter("--warn-reserved");
        command.addParameter("--warn-library");
        command.addParameter("--warn-vital-generic");
        command.addParameter("--warn-delayed-checks");
        command.addParameter("--warn-body");
        command.addParameter("--warn-specs");
        command.addParameter("--warn-unused");
        command.addParameter("--workdir=" + targetPath.getAbsolutePath());

        command.addParameter(sourcePath);
        command.setEnvParams(new HashMap<String, String>() {{
            //  put("SOME_ENV", "some-value");
        }});

        CompilationTaskWorker compilationTaskWorker =
                new CompilationTaskWorker(new VhdlCompilerOutputStreamParser(executionPath));
        if (compilationTaskWorker.executeTask(command, executionPath, context) == null) {
            return;
        }

        Collection<OutputItem> outputItems = new ArrayList<OutputItem>();

        sink.add(targetPath.getAbsolutePath(), outputItems, VirtualFile.EMPTY_ARRAY);
    }

    private String getGhdlToolBinary() {
        final VhdlState state = ServiceManager.getService(VhdlSettings.class).getState();
        return (state != null ? state : VhdlSettings.getDefaults()).getGhdlToolBinary();
    }

    @Override
    public boolean isCompilableFile(VirtualFile file, CompileContext context) {
        return file.getFileType().equals(VhdlFileType.INSTANCE);
    }

    @NotNull
    @Override
    public String getDescription() {
        return COMPILER_DESC;
    }

    @Override
    public boolean validateConfiguration(CompileScope scope) {
        return true;
    }
}
