package com.staremax.vhdlplug.compiler;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import com.staremax.vhdlplug.bundle.CompilerBundle;
import com.staremax.vhdlplug.bundle.VhdlBundle;
import com.staremax.vhdlplug.ide.notification.VhdlNotificationUtil;
import com.staremax.vhdlplug.logger.Logger;

import java.io.File;
import java.io.PrintWriter;

/**
 * Auxiliary class. Creates a Makefiles.
 */
public class CompilerUtil {

    private static final Logger LOG = Logger.getLogger(CompilerUtil.class.getName());

    public static void generateLocalMakefile(Project project, String localMakefile, String localMakefileSrc) {
        String basePath = project.getBaseDir().getPath();
        File makefile = new File(basePath, localMakefile);
        PrintWriter writer = null;

        try {
            final String content = CompilerBundle.message("makefile-local", localMakefileSrc);
            writer = new PrintWriter(makefile);
            writer.write(content);
        } catch (Exception e) {
            LOG.warn("File Error", e);
            Messages.showErrorDialog(project, e.getMessage(), "File Error");
        } finally {
            if (writer != null) writer.close();
        }
    }

    public static void generateSourcesForMakefile(Project project, VirtualFile[] files, String localMakefileSrc) {
        String basePath = project.getBaseDir().getPath();
        File targetPath = new File(basePath, "out");
        if (!targetPath.exists() && !targetPath.mkdirs()) {
            LOG.warn("Could not create parent dirs: " + targetPath);
        }

        File makefileSrc = new File(basePath, "/out/" + localMakefileSrc);
        PrintWriter writer = null;

        StringBuilder content = new StringBuilder("SOURCES=");
        for (VirtualFile f : files) {
            content.append(f.getPath()).append(' ');
        }

        try {
            writer = new PrintWriter(makefileSrc);
            writer.write(content.toString());
        } catch (Exception e) {
            LOG.warn("File Error", e);
            Messages.showErrorDialog(project, e.getMessage(), "File Error");
        } finally {
            if (writer != null) writer.close();
        }
    }

    public static void generateMakefileForCI(Project project) {
        final String MAKEFILE = "Makefile";

        String basePath = project.getBaseDir().getPath();
        File makefile = new File(basePath, MAKEFILE);
        PrintWriter writer = null;

        try {
            final String content = CompilerBundle.message("makefile-ci");

            writer = new PrintWriter(makefile);
            writer.write(content);

            final String title = VhdlBundle.message("vhdl.notification.title.info");
            final String message = VhdlBundle.message("vhdl.makefile.generate.successful");
            VhdlNotificationUtil.busInfoFireNotification(title, message);
        } catch (Exception e) {
            LOG.warn("File Error", e);
            Messages.showErrorDialog(project, e.getMessage(), "File Error");
        } finally {
            if (writer != null) writer.close();
        }
    }
}
