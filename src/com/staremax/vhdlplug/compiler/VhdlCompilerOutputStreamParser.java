package com.staremax.vhdlplug.compiler;

import com.intellij.openapi.compiler.CompilerMessageCategory;
import com.staremax.vhdlplug.utils.ProcessUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parsing the output stream and creating compiler messages
 */
public class VhdlCompilerOutputStreamParser implements ProcessUtil.StreamParser<List<VhdlCompilerMessage>> {

    private final static Pattern errorPattern = Pattern.compile("((\\w:)?[^:]+):(\\d+):(\\d+):\\s*(.*)", Pattern.UNIX_LINES);
    private final static Pattern warningPattern = Pattern.compile("((\\w:)?[^:]+):(\\d+):(\\d+):warning:\\s*(.*)", Pattern.UNIX_LINES);
    private final String basePath;

    public VhdlCompilerOutputStreamParser(String basePath) {
        this.basePath = basePath;
    }

    public List<VhdlCompilerMessage> parseStream(String data) {
        List<VhdlCompilerMessage> messages = new ArrayList<VhdlCompilerMessage>();
        CompilerMessageCategory category;
        int line = -1;
        int column = -1;
        String message = null;
        String url = null;
        boolean isMatched = false;

        data = data.trim();
        Matcher matcher = warningPattern.matcher(data);
        if (matcher.find()) {
            isMatched = true;
            category = CompilerMessageCategory.WARNING;
        } else {
            matcher = errorPattern.matcher(data);
            if (matcher.find()) {
                isMatched = true;
            } else {
                message = data;
            }
            category = CompilerMessageCategory.ERROR;
        }

        if (isMatched) {
            url = CompilationTaskWorker.pathToUrl(matcher.group(1));
            line = Integer.parseInt(matcher.group(3));
            column = Integer.parseInt(matcher.group(4));
            message = matcher.group(5);
        }

        if (message != null && !message.isEmpty()) {
            messages.add(new VhdlCompilerMessage(category, message, url, line, column));
        }

        return messages;
    }
}
