package com.staremax.vhdlplug.ide.notification;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;

/**
 * Shows pop-up notifications
 */
public class VhdlNotificationUtil {

    private static final String NOTIFICATION_GROUP_ID = "Vhdl Notification";

    public static void busInfoFireNotification(String title, String content) {
        showNotification(title, content, NotificationType.INFORMATION);
    }

    public static void busErrorFireNotification(String title, String content) {
        showNotification(title, content, NotificationType.ERROR);
    }

    public static void busWarnFireNotification(String title, String content) {
        showNotification(title, content, NotificationType.WARNING);
    }

    private static void showNotification(String title, String content, NotificationType type) {
        Notification notification = new Notification(NOTIFICATION_GROUP_ID, title, content, type);
        Notifications.Bus.notify(notification);
    }
}
