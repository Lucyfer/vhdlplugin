package com.staremax.vhdlplug.ide.module;

import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.module.ModuleTypeManager;
import icons.VhdlIcons;

import javax.swing.*;

/**
 * Defines VHDL module type
 */
public class VhdlModuleType extends ModuleType<VhdlModuleBuilder> {

    public static final String MODULE_TYPE_ID = "VHDL_MODULE";
    public static final String MODULE_NAME = "VHDL Module";
    public static final String MODULE_DESC = "A module supporting VHDL development";

    public VhdlModuleType() {
        super(MODULE_TYPE_ID);
    }

    public static VhdlModuleType getInstance() {
        return (VhdlModuleType) ModuleTypeManager.getInstance().findByID(MODULE_TYPE_ID);
    }

    @Override
    public VhdlModuleBuilder createModuleBuilder() {
        return new VhdlModuleBuilder();
    }

    @Override
    public String getName() {
        return MODULE_NAME;
    }

    @Override
    public String getDescription() {
        return MODULE_DESC;
    }

    @Override
    public Icon getBigIcon() {
        return VhdlIcons.VhdlBigIcon;
    }

    @Override
    public Icon getNodeIcon(@Deprecated boolean isOpened) {
        return VhdlIcons.VhdlIcon;
    }
}
