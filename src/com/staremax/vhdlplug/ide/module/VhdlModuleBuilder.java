package com.staremax.vhdlplug.ide.module;

import com.intellij.ide.util.projectWizard.JavaModuleBuilder;
import com.intellij.ide.util.projectWizard.ModuleBuilderListener;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.projectRoots.SdkTypeId;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiManager;
import com.staremax.vhdlplug.ide.template.VhdlTemplatesFactory;
import org.jetbrains.annotations.NotNull;

/**
 * Defines a VHDL project module.
 */
public class VhdlModuleBuilder extends JavaModuleBuilder implements ModuleBuilderListener {

    public VhdlModuleBuilder() {
        addListener(this);
    }

    @Override
    public void moduleCreated(@NotNull Module module) {
        ModuleRootManager moduleRootManager = ModuleRootManager.getInstance(module);
        VirtualFile sourceRoots[] = moduleRootManager.getSourceRoots();

        if (sourceRoots.length == 1) {
            PsiDirectory directory = PsiManager.getInstance(module.getProject()).findDirectory(sourceRoots[0]);
            if (directory != null) {
                VhdlTemplatesFactory.createFromTemplate(directory, "main", "main.vhdl", VhdlTemplatesFactory.VhdlTemplate.VhdlEntityFile);
            }
        }
    }

    @Override
    public ModuleType getModuleType() {
        return VhdlModuleType.getInstance();
    }

    /**
     * No sdk dependencies
     *
     * @param sdkType sdk
     * @return always true
     */
    @Override
    public boolean isSuitableSdkType(SdkTypeId sdkType) {
        return true;
    }
}
