package com.staremax.vhdlplug.ide.template;

import com.intellij.ide.fileTemplates.*;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiFileFactory;
import com.staremax.vhdlplug.VhdlFile;
import com.staremax.vhdlplug.VhdlFileType;
import icons.VhdlIcons;

import java.util.Properties;

/**
 * Defines a file templates.
 */
public class VhdlTemplatesFactory implements FileTemplateGroupDescriptorFactory {

    public static final String FILE_TEMPLATES = "Vhdl File Templates";

    public enum VhdlTemplate {
        VhdlPackageFile("VhdlPackageFile.vhdl"), VhdlEntityFile("VhdlEntityFile.vhdl");

        final String file;

        VhdlTemplate(String file) {
            this.file = file;
        }

        public String getFile() {
            return file;
        }
    }

    @Override
    public FileTemplateGroupDescriptor getFileTemplatesDescriptor() {
        final FileTemplateGroupDescriptor group =
                new FileTemplateGroupDescriptor(FILE_TEMPLATES, VhdlIcons.VhdlIcon);
        for (VhdlTemplate template : VhdlTemplate.values()) {
            group.addTemplate(new FileTemplateDescriptor(template.getFile(), VhdlIcons.VhdlIcon));
        }

        return group;
    }

    public static VhdlFile createFromTemplate(PsiDirectory directory, String packageName, String fileName, VhdlTemplate template) {
        final FileTemplate fileTemplate = FileTemplateManager.getInstance().getTemplate(template.getFile());
        final Properties properties = new Properties(FileTemplateManager.getInstance().getDefaultProperties());
        properties.setProperty("PACKAGE_NAME", packageName);
        String text;
        try {
            text = fileTemplate.getText(properties);
        } catch (Exception e) {
            throw new RuntimeException("Unable to load template for " + template.getFile(), e);
        }
        final PsiFileFactory factory = PsiFileFactory.getInstance(directory.getProject());
        final PsiFile file = factory.createFileFromText(fileName, VhdlFileType.INSTANCE, text);

        return (VhdlFile) directory.add(file);
    }
}
