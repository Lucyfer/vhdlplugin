package com.staremax.vhdlplug.ide.intention;

import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFile;
import com.intellij.util.IncorrectOperationException;
import com.staremax.vhdlplug.psi.VhdlEndNamedElement;
import com.staremax.vhdlplug.psi.VhdlNamedElement;
import org.jetbrains.annotations.NotNull;

/**
 * Quick fix unmatched endnames.
 */
public class FixEndNameIntention extends BaseIntentionAction {

    private VhdlEndNamedElement element;

    public FixEndNameIntention(VhdlEndNamedElement element) {
        this.element = element;
    }

    @NotNull
    @Override
    public String getText() {
        return "Fix end name";
    }

    @NotNull
    @Override
    public String getFamilyName() {
        return IntentionConstants.VHDL_SYNTAX_FAMILY_INTENTION;
    }

    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, PsiFile file) throws IncorrectOperationException {
        final String name = element.getMainName().getName();
        final VhdlNamedElement endNameElement = element.getEndName();
        if (endNameElement != null && name != null) {
            endNameElement.setName(name);
        }
    }
}
