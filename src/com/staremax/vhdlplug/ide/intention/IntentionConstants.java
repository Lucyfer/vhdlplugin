package com.staremax.vhdlplug.ide.intention;

interface IntentionConstants {
    String VHDL_SYNTAX_FAMILY_INTENTION = "Vhdl Syntax Family Intention";
}
