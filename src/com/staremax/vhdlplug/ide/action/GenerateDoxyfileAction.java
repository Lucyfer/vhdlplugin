package com.staremax.vhdlplug.ide.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.staremax.vhdlplug.documentation.DoxygenUtil;

public class GenerateDoxyfileAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        final Project project = e.getProject();

        DoxygenUtil.generateDoxyfile(project);
    }
}
