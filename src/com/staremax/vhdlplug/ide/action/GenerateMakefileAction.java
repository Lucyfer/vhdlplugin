package com.staremax.vhdlplug.ide.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.staremax.vhdlplug.compiler.CompilerUtil;
import com.staremax.vhdlplug.compiler.GhdlMakefileCompiler;

public class GenerateMakefileAction extends AnAction {
    public void actionPerformed(AnActionEvent e) {
        final Project project = e.getProject();

        CompilerUtil.generateLocalMakefile(project,
                GhdlMakefileCompiler.MAKEFILE_LOCAL,
                GhdlMakefileCompiler.MAKEFILE_LOCAL_SRC);
        CompilerUtil.generateMakefileForCI(project);
    }
}
