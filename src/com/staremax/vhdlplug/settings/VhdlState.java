package com.staremax.vhdlplug.settings;

/**
 * JavaBean class for holding Vhdl Settings
 */
public class VhdlState {

    /**
     * Path to GHDL toolkit binary
     */
    private String ghdlToolBinary;

    /**
     * Path to Make build tool binary
     */
    private String makeToolBinary;

    /**
     * Use generated Makefile or use internal compiler
     */
    private boolean useMakefile;

    public VhdlState() {
        // defaults
        this.ghdlToolBinary = "ghdl";
        this.makeToolBinary = "make";
        this.useMakefile = true;
    }

    public String getGhdlToolBinary() {
        return ghdlToolBinary;
    }

    public void setGhdlToolBinary(String ghdlToolBinary) {
        this.ghdlToolBinary = ghdlToolBinary;
    }

    public String getMakeToolBinary() {
        return makeToolBinary;
    }

    public void setMakeToolBinary(String makeToolBinary) {
        this.makeToolBinary = makeToolBinary;
    }

    public boolean isUseMakefile() {
        return useMakefile;
    }

    public void setUseMakefile(boolean useMakefile) {
        this.useMakefile = useMakefile;
    }
}
