package com.staremax.vhdlplug.settings;

import com.intellij.openapi.components.*;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@State(
        name = "VhdlSettings",
        storages = {
                @Storage(
                        id = "other",
                        file = StoragePathMacros.APP_CONFIG + "/other.xml")
        })
public class VhdlSettings implements PersistentStateComponent<VhdlState>, ApplicationComponent {

    public static final String COMPONENT_NAME = "VhdlSettingsComponent";

    private VhdlState state;

    public VhdlSettings() {
        this.state = new VhdlState();
    }

    public static VhdlState getDefaults() {
        return new VhdlState();
    }

    @Nullable
    @Override
    public VhdlState getState() {
        return state;
    }

    @Override
    public void loadState(VhdlState state) {
        XmlSerializerUtil.copyBean(state, this.state);
    }

    @NotNull
    @Override
    public String getComponentName() {
        return COMPONENT_NAME;
    }

    @Override
    public void initComponent() {
        // do nothing
    }

    @Override
    public void disposeComponent() {
        // do nothing
    }
}
