package com.staremax.vhdlplug.settings.ui;

import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.staremax.vhdlplug.settings.VhdlSettings;
import com.staremax.vhdlplug.settings.VhdlState;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class VhdlConfigForm implements Configurable {

    public static final String CONFIG_NAME = "VHDL Plugin Config";

    private JTextField makePathField;
    private JTextField ghdlPathField;
    private JCheckBox useMakefileCheckBox;
    private JPanel root;

    /**
     * Instantiate State class
     *
     * @return Vhdl Settings holding class
     */
    VhdlState getCurrentState() {
        return ServiceManager.getService(VhdlSettings.class).getState();
    }

    @Nls
    @Override
    public String getDisplayName() {
        return CONFIG_NAME;
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        return this.root;
    }

    /**
     * Checks if the settings in the user interface component were modified by the user and
     * need to be saved.
     */
    @Override
    public boolean isModified() {
        final VhdlState state = getCurrentState();
        final String pathToMake = makePathField.getText() != null ? makePathField.getText() : "";
        final String pathToGhdl = ghdlPathField.getText() != null ? ghdlPathField.getText() : "";
        final boolean useMake = useMakefileCheckBox.isSelected();

        return !pathToMake.equals(state.getMakeToolBinary())
                || !pathToGhdl.equals(state.getGhdlToolBinary())
                || useMake != state.isUseMakefile();
    }

    /**
     * Store the settings from configurable to other components.
     */
    @Override
    public void apply() throws ConfigurationException {
        final VhdlState state = getCurrentState();

        state.setMakeToolBinary(makePathField.getText());
        state.setGhdlToolBinary(ghdlPathField.getText());
        state.setUseMakefile(useMakefileCheckBox.isSelected());
    }

    /**
     * Load settings from other components to configurable.
     */
    @Override
    public void reset() {
        final VhdlState state = getCurrentState();

        makePathField.setText(state.getMakeToolBinary());
        ghdlPathField.setText(state.getGhdlToolBinary());
        useMakefileCheckBox.setSelected(state.isUseMakefile());
    }

    /**
     * Disposes the Swing components used for displaying the configuration.
     */
    @Override
    public void disposeUIResources() {
    }
}
