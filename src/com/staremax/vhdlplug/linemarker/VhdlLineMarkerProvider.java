package com.staremax.vhdlplug.linemarker;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.psi.VhdlArchitectureBody;
import com.staremax.vhdlplug.psi.VhdlEntityDeclaration;
import com.staremax.vhdlplug.utils.VhdlUtil;
import icons.VhdlIcons;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Defines a markers (icons on the left gutter)
 */
public class VhdlLineMarkerProvider extends RelatedItemLineMarkerProvider {

    @Override
    protected void collectNavigationMarkers(@NotNull PsiElement element, Collection<? super RelatedItemLineMarkerInfo> result) {
        if (element instanceof VhdlArchitectureBody) {
            VhdlArchitectureBody body = (VhdlArchitectureBody) element;
            String entityName = body.getEntityNameString();
            Collection<VhdlEntityDeclaration> entities = VhdlUtil.findEntities(element.getProject(), entityName);
            if (!entities.isEmpty()) {
                NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(VhdlIcons.VhdlImplIcon)
                        .setTargets(entities).setTooltipText("Navigate to a Entity declaration");
                result.add(builder.createLineMarkerInfo(element));
            }
        } else if (element instanceof VhdlEntityDeclaration) {
            VhdlEntityDeclaration entity = (VhdlEntityDeclaration) element;
            Collection<VhdlArchitectureBody> implementations =
                    VhdlUtil.findArchitecturesOfEntity(element.getProject(), entity.getEntityNameString());
            if (!implementations.isEmpty()) {
                NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(VhdlIcons.VhdlEntityIcon)
                        .setTargets(implementations).setTooltipText("Navigate to a Architecture body");
                result.add(builder.createLineMarkerInfo(element));
            }
        }
    }
}
