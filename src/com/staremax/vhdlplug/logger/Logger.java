package com.staremax.vhdlplug.logger;

public class Logger {

    public static final int DEBUG = 0;
    public static final int INFO = 1;
    public static final int WARN = 2;
    public static final int ERROR = 3;

    private static int LEVEL;
    private String className;

    static {
        LEVEL = DEBUG;
    }

    private Logger(String className) {
        this.className = className;
    }

    public static Logger getLogger(String className) {
        return new Logger(className);
    }

    public boolean isDebugEnabled() {
        return LEVEL == DEBUG;
    }

    public void debug(String message) {
        if (LEVEL <= DEBUG) {
            System.out.println("DEBUG: " + className + ": " + message);
        }
    }

    public void debug(String message, Exception e) {
        if (LEVEL <= DEBUG) {
            System.out.println("DEBUG: " + className + ": " + message);
            System.out.println("\tInner exception: " + e.getMessage());
        }
    }

    public void info(String message) {
        if (LEVEL <= INFO) {
            System.out.println("INFO: " + className + ": " + message);
        }
    }

    public void info(String message, Exception e) {
        if (LEVEL <= INFO) {
            System.out.println("INFO: " + className + ": " + message);
            System.out.println("\tInner exception: " + e.getMessage());
        }
    }

    public void warn(String message) {
        if (LEVEL <= WARN) {
            System.out.println("WARN: " + className + ": " + message);
        }
    }

    public void warn(String message, Exception e) {
        if (LEVEL <= WARN) {
            System.out.println("WARN: " + className + ": " + message);
            System.out.println("\tInner exception: " + e.getMessage());
        }
    }

    public void error(String message) {
        if (LEVEL <= ERROR) {
            System.out.println("ERROR: " + className + ": " + message);
        }
    }

    public void error(String message, Exception e) {
        if (LEVEL <= ERROR) {
            System.out.println("ERROR: " + className + ": " + message);
            System.out.println("\tInner exception: " + e.getMessage());
        }
    }
}
