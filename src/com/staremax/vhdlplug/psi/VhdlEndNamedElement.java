package com.staremax.vhdlplug.psi;

import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface VhdlEndNamedElement extends PsiElement {

    @NotNull
    VhdlNamedElement getMainName();

    @Nullable
    VhdlNamedElement getEndName();
}
