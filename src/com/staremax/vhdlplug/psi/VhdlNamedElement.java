package com.staremax.vhdlplug.psi;

import com.intellij.psi.PsiNameIdentifierOwner;

public interface VhdlNamedElement extends PsiNameIdentifierOwner {
}
