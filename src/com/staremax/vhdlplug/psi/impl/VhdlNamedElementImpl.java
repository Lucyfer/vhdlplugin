package com.staremax.vhdlplug.psi.impl;

import com.intellij.lang.ASTNode;
import com.staremax.vhdlplug.psi.VhdlASTWrapperPsiElement;
import com.staremax.vhdlplug.psi.VhdlNamedElement;
import org.jetbrains.annotations.NotNull;

public abstract class VhdlNamedElementImpl extends VhdlASTWrapperPsiElement implements VhdlNamedElement {
    public VhdlNamedElementImpl(@NotNull ASTNode node) {
        super(node);
    }

    @Override
    public String getName() {
        return getText();
    }
}
