package com.staremax.vhdlplug.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.lexer.VhdlTokenTypesExtension;
import com.staremax.vhdlplug.navigation.presentation.VhdlArchitecturePresentation;
import com.staremax.vhdlplug.navigation.presentation.VhdlEntityPresentation;
import com.staremax.vhdlplug.navigation.presentation.VhdlPackageBodyPresentation;
import com.staremax.vhdlplug.navigation.presentation.VhdlPackagePresentation;
import com.staremax.vhdlplug.psi.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VhdlPsiImplUtil {

    @NotNull
    public static VhdlNamedElement getMainName(@NotNull VhdlEntityDeclaration element) {
        return element.getEntityName();
    }

    @Nullable
    public static VhdlNamedElement getEndName(@NotNull VhdlEntityDeclaration element) {
        return element.getEndEntityName();
    }

    @NotNull
    public static String getEntityNameString(@NotNull VhdlEntityDeclaration element) {
        ASTNode name = element.getEntityName().getNode();
        if (name != null) {
            return name.getText();
        }
        return "";
    }

    public static void delete(@NotNull VhdlEntityDeclaration element) {
        final ASTNode parentNode = element.getParent().getNode();
        assert parentNode != null;

        ASTNode node = element.getNode();
        ASTNode prev = node.getTreePrev();
        ASTNode next = node.getTreeNext();
        parentNode.removeChild(node);
        if ((prev == null || prev.getElementType() == VhdlTokenTypesExtension.WHITESPACE) && next != null &&
                next.getElementType() == VhdlTokenTypesExtension.WHITESPACE) {
            parentNode.removeChild(next);
        }
    }

    @NotNull
    public static String getName(@NotNull VhdlEntityDeclaration element) {
        return element.getEntityName().getText();
    }

    @NotNull
    public static PsiElement setName(@NotNull VhdlEntityDeclaration element, String newName) {
        ASTNode nameNode = element.getEntityName().getNode();
        if (nameNode != null) {
            VhdlEntityName newNameNode = VhdlElementFactory.createEntityName(element.getProject(), newName);
            element.getEntityName().replace(newNameNode);
        }
        return element;
    }

    @NotNull
    public static PsiElement getNameIdentifier(@NotNull VhdlEntityDeclaration element) {
        return element.getEntityName();
    }

    @NotNull
    public static String getName(@NotNull VhdlArchitectureBody element) {
        return element.getArchitectureNameString();
    }

    @NotNull
    public static PsiElement setName(@NotNull VhdlArchitectureBody element, @NotNull String newName) {
        ASTNode nameNode = element.getArchitectureName().getNode();
        if (nameNode != null) {
            VhdlArchitectureName newNameNode = VhdlElementFactory.createArchitectureName(element.getProject(), newName);
            nameNode.replaceAllChildrenToChildrenOf(newNameNode.getNode());
        }
        return element;
    }

    @NotNull
    public static String getEntityNameString(@NotNull VhdlArchitectureBody element) {
        return element.getEntityName().getText();
    }

    @NotNull
    public static String getArchitectureNameString(@NotNull VhdlArchitectureBody element) {
        return element.getArchitectureName().getText();
    }

    @NotNull
    public static PsiElement getNameIdentifier(@NotNull VhdlArchitectureBody element) {
        return element.getArchitectureName();
    }

    public static void delete(@NotNull VhdlArchitectureBody element) {
        final ASTNode parentNode = element.getParent().getNode();
        assert parentNode != null;

        ASTNode node = element.getNode();
        ASTNode prev = node.getTreePrev();
        ASTNode next = node.getTreeNext();
        parentNode.removeChild(node);
        if ((prev == null || prev.getElementType() == VhdlTokenTypesExtension.WHITESPACE) && next != null &&
                next.getElementType() == VhdlTokenTypesExtension.WHITESPACE) {
            parentNode.removeChild(next);
        }
    }

    @NotNull
    public static VhdlNamedElement getMainName(@NotNull VhdlArchitectureBody element) {
        return element.getArchitectureName();
    }

    @Nullable
    public static VhdlNamedElement getEndName(@NotNull VhdlArchitectureBody element) {
        return element.getEndArchitectureName();
    }

    @NotNull
    public static String getName(@NotNull VhdlEntityName element) {
        return element.getText();
    }

    @NotNull
    public static PsiElement setName(@NotNull VhdlEntityName element, @NotNull String newName) {
        ASTNode nameNode = element.getNode();
        if (nameNode != null) {
            VhdlEntityName newNameNode = VhdlElementFactory.createEntityName(element.getProject(), newName);
            nameNode.replaceAllChildrenToChildrenOf(newNameNode.getNode());
        }
        return element;
    }

    @NotNull
    public static PsiElement getNameIdentifier(@NotNull VhdlEndEntityName element) {
        return element;
    }

    @NotNull
    public static String getName(@NotNull VhdlEndEntityName element) {
        return element.getText();
    }

    @NotNull
    public static PsiElement setName(@NotNull VhdlEndEntityName element, @NotNull String newName) {
        ASTNode nameNode = element.getNode();
        if (nameNode != null) {
            VhdlEndEntityName newNameNode = VhdlElementFactory.createEndEntityName(element.getProject(), newName);
            nameNode.replaceAllChildrenToChildrenOf(newNameNode.getNode());
        }
        return element;
    }

    @NotNull
    public static PsiElement getNameIdentifier(@NotNull VhdlEntityName element) {
        return element;
    }

    @NotNull
    public static PsiElement getNameIdentifier(@NotNull VhdlArchitectureName element) {
        return element;
    }

    @NotNull
    public static PsiElement getNameIdentifier(@NotNull VhdlEndArchitectureName element) {
        return element;
    }

    @NotNull
    public static PsiElement setName(@NotNull VhdlEndArchitectureName element, @NotNull String newName) {
        ASTNode nameNode = element.getNode();
        if (nameNode != null) {
            VhdlEndArchitectureName newNameNode = VhdlElementFactory.createEndArchitectureName(element.getProject(), newName);
            nameNode.replaceAllChildrenToChildrenOf(newNameNode.getNode());
        }
        return element;
    }

    @NotNull
    public static PsiElement setName(@NotNull VhdlArchitectureName element, @NotNull String newName) {
        ASTNode nameNode = element.getNode();
        if (nameNode != null) {
            VhdlArchitectureName newNameNode = VhdlElementFactory.createArchitectureName(element.getProject(), newName);
            nameNode.replaceAllChildrenToChildrenOf(newNameNode.getNode());
        }
        return element;
    }

    @NotNull
    public static PsiElement setName(@NotNull VhdlSimpleName element, @NotNull String newName) {
        VhdlSimpleName newNameNode = VhdlElementFactory.createSimpleName(element.getProject(), newName);
        return element.replace(newNameNode);
    }

    @NotNull
    public static PsiElement getNameIdentifier(@NotNull VhdlSimpleName element) {
        return element;
    }

    @NotNull
    public static PsiElement setName(@NotNull VhdlEndSimpleName element, @NotNull String newName) {
        VhdlEndSimpleName newNameNode = VhdlElementFactory.createEndSimpleName(element.getProject(), newName);
        return element.replace(newNameNode);
    }

    @NotNull
    public static PsiElement getNameIdentifier(@NotNull VhdlEndSimpleName element) {
        return element;
    }

    @NotNull
    public static VhdlNamedElement getMainName(@NotNull VhdlPackageDeclaration element) {
        return element.getSimpleName();
    }

    @Nullable
    public static VhdlNamedElement getEndName(@NotNull VhdlPackageDeclaration element) {
        return element.getEndSimpleName();
    }

    @NotNull
    public static VhdlNamedElement getMainName(@NotNull VhdlPackageBody element) {
        return element.getSimpleName();
    }

    @Nullable
    public static VhdlNamedElement getEndName(@NotNull VhdlPackageBody element) {
        return element.getEndSimpleName();
    }

    @NotNull
    public static ItemPresentation getPresentation(@NotNull final VhdlEntityDeclaration element) {
        return new VhdlEntityPresentation(element);
    }

    @NotNull
    public static ItemPresentation getPresentation(@NotNull final VhdlArchitectureBody element) {
        return new VhdlArchitecturePresentation(element);
    }

    @NotNull
    public static ItemPresentation getPresentation(@NotNull final VhdlPackageDeclaration element) {
        return new VhdlPackagePresentation(element);
    }

    @NotNull
    public static ItemPresentation getPresentation(@NotNull final VhdlPackageBody element) {
        return new VhdlPackageBodyPresentation(element);
    }
}
