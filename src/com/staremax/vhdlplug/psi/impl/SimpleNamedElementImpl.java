package com.staremax.vhdlplug.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiNamedElement;
import com.staremax.vhdlplug.psi.VhdlASTWrapperPsiElement;
import org.jetbrains.annotations.NotNull;

public abstract class SimpleNamedElementImpl extends VhdlASTWrapperPsiElement implements PsiNamedElement {
    public SimpleNamedElementImpl(@NotNull ASTNode astNode) {
        super(astNode);
    }

    @Override
    public String getName() {
        return getText();
    }
}
