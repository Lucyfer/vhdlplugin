package com.staremax.vhdlplug.psi;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFileFactory;
import com.staremax.vhdlplug.VhdlFile;
import com.staremax.vhdlplug.VhdlFileType;
import org.jetbrains.annotations.NotNull;

/**
 * Util class for generating PSI-trees.
 */
public final class VhdlElementFactory {

    @NotNull
    public static VhdlSimpleName createSimpleName(Project project, String newName) {
        VhdlPackageDeclaration p = createPackageDeclaration(project, newName );
        return p.getSimpleName();
    }

    @NotNull
    public static VhdlEndSimpleName createEndSimpleName(Project project, String newName) {
        VhdlPackageDeclaration p = createPackageDeclaration(project, newName );
        VhdlEndSimpleName name = p.getEndSimpleName();
        if (name != null) return name;
        throw new NullPointerException();
    }

    @NotNull
    public static VhdlArchitectureName createArchitectureName(Project project, String name) {
        VhdlArchitectureBody arch = createArchitecture(project, name);
        return arch.getArchitectureName();
    }

    @NotNull
    public static VhdlEndArchitectureName createEndArchitectureName(Project project, String name) {
        VhdlArchitectureBody arch = createArchitecture(project, name);
        VhdlEndArchitectureName archName = arch.getEndArchitectureName();
        if (archName != null) return archName;
        throw new NullPointerException();
    }

    @NotNull
    public static VhdlEntityName createEntityName(Project project, String name) {
        VhdlEntityDeclaration entity = createEntity(project, name);
        return entity.getEntityName();
    }

    @NotNull
    public static VhdlEndEntityName createEndEntityName(Project project, String name) {
        VhdlEntityDeclaration entity = createEntity(project, name);
        VhdlEndEntityName endEntityName = entity.getEndEntityName();
        if (endEntityName != null) return endEntityName;
        throw new NullPointerException();
    }

    @NotNull
    public static VhdlPackageDeclaration createPackageDeclaration(Project project, String name) {
        final String text = "PACKAGE " + name + " IS END " + name + ";";

        VhdlDesignUnit designUnit = createVhdlFile(project, text).findChildByClass(VhdlDesignUnit.class);
        if (designUnit != null) {
            VhdlPrimaryUnit unit = designUnit.getLibraryUnit().getPrimaryUnit();
            if (unit != null) {
                VhdlPackageDeclaration packageDeclaration = unit.getPackageDeclaration();
                if (packageDeclaration != null) return packageDeclaration;
            }
        }
        throw new NullPointerException();
    }

    @NotNull
    public static VhdlEntityDeclaration createEntity(Project project, String name) {
        final String text = "ENTITY " + name + " IS END " + name + ";";

        VhdlDesignUnit designUnit = createVhdlFile(project, text).findChildByClass(VhdlDesignUnit.class);
        if (designUnit != null) {
            VhdlPrimaryUnit unit = designUnit.getLibraryUnit().getPrimaryUnit();
            if (unit != null) {
                VhdlEntityDeclaration entity = unit.getEntityDeclaration();
                if (entity != null) return entity;
            }
        }
        throw new NullPointerException();
    }

    @NotNull
    public static VhdlArchitectureBody createArchitecture(Project project, String name) {
        final String text = "ARCHITECTURE " + name + " OF dummy IS BEGIN END " + name + ";";

        VhdlDesignUnit designUnit = createVhdlFile(project, text).findChildByClass(VhdlDesignUnit.class);
        if (designUnit != null) {
            VhdlSecondaryUnit unit = designUnit.getLibraryUnit().getSecondaryUnit();
            if (unit != null) {
                VhdlArchitectureBody arch = unit.getArchitectureBody();
                if (arch != null) return arch;
            }
        }
        throw new NullPointerException();
    }

    @NotNull
    private static VhdlFile createVhdlFile(Project project, String text) {
        PsiFileFactory factory = PsiFileFactory.getInstance(project);
        return (VhdlFile) factory.createFileFromText("dummy.vhdl", VhdlFileType.INSTANCE, text);
    }
}
