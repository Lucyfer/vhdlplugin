package com.staremax.vhdlplug.reference;

import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.PsiReferenceContributor;
import com.intellij.psi.PsiReferenceRegistrar;
import com.staremax.vhdlplug.psi.*;
import com.staremax.vhdlplug.reference.provider.VhdlArchitectureReferenceProvider;
import com.staremax.vhdlplug.reference.provider.VhdlEntityReferenceProvider;
import com.staremax.vhdlplug.reference.provider.VhdlSimpleNameReferenceProvider;

public class VhdlReferenceContributor extends PsiReferenceContributor {

    @Override
    @SuppressWarnings("unchecked")
    public void registerReferenceProviders(PsiReferenceRegistrar registrar) {
        registrar.registerReferenceProvider(
                PlatformPatterns.or(
                        PlatformPatterns.psiElement(VhdlEntityName.class),
                        PlatformPatterns.psiElement(VhdlEndEntityName.class)),
                new VhdlEntityReferenceProvider());

        registrar.registerReferenceProvider(
                PlatformPatterns.or(
                        PlatformPatterns.psiElement(VhdlArchitectureName.class),
                        PlatformPatterns.psiElement(VhdlEndArchitectureName.class)),
                new VhdlArchitectureReferenceProvider());

        registrar.registerReferenceProvider(
                PlatformPatterns.or(
                        PlatformPatterns.psiElement(VhdlSimpleName.class),
                        PlatformPatterns.psiElement(VhdlEndSimpleName.class)),
                new VhdlSimpleNameReferenceProvider());
    }
}
