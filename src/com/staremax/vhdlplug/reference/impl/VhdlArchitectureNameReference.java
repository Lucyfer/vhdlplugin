package com.staremax.vhdlplug.reference.impl;

import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.psi.VhdlArchitectureName;
import com.staremax.vhdlplug.reference.VhdlReference;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VhdlArchitectureNameReference extends VhdlReference {

    public VhdlArchitectureNameReference(@NotNull VhdlArchitectureName element) {
        super(element);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        return myElement.getParent();
    }
}
