package com.staremax.vhdlplug.reference.impl;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.psi.VhdlEntityDeclaration;
import com.staremax.vhdlplug.psi.VhdlEntityName;
import com.staremax.vhdlplug.reference.VhdlReference;
import com.staremax.vhdlplug.utils.VhdlUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class VhdlEntityNameReference extends VhdlReference {

    public VhdlEntityNameReference(@NotNull VhdlEntityName element) {
        super(element);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        Project project = myElement.getProject();
        Collection<VhdlEntityDeclaration> entities = VhdlUtil.findEntities(project, myElement.getText());
        if (entities.size() == 1) {
            return entities.iterator().next();
        } else {
            return null;
        }
    }

    @NotNull
    @Override
    public Object[] getVariants() {
        Project project = myElement.getProject();
        List<LookupElement> variants = new ArrayList<LookupElement>();
        Collection<VhdlEntityDeclaration> entities = VhdlUtil.findEntities(project);
        for (VhdlEntityDeclaration e : entities) {
            variants.add(LookupElementBuilder.create(e).withTypeText(e.getContainingFile().getName()));
        }
        return variants.toArray();
    }
}
