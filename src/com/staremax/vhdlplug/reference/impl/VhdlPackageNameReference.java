package com.staremax.vhdlplug.reference.impl;

import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.psi.VhdlPackageDeclaration;
import com.staremax.vhdlplug.reference.VhdlReference;
import com.staremax.vhdlplug.utils.VhdlUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public class VhdlPackageNameReference extends VhdlReference {

    public VhdlPackageNameReference(@NotNull PsiElement element) {
        super(element);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        Collection<VhdlPackageDeclaration> p = VhdlUtil.findPackages(myElement.getProject(), myElement.getText());
        if (p.size() == 1) {
            return p.iterator().next().getSimpleName();
        }
        return null;
    }
}
