package com.staremax.vhdlplug.reference.impl;

import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.psi.VhdlEndSimpleName;
import com.staremax.vhdlplug.reference.VhdlReference;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VhdlEndSimpleNameReference extends VhdlReference {

    public VhdlEndSimpleNameReference(@NotNull VhdlEndSimpleName element) {
        super(element);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        // Stub
        return null;
    }
}
