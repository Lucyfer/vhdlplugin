package com.staremax.vhdlplug.reference.impl;

import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.psi.VhdlArchitectureBody;
import com.staremax.vhdlplug.psi.VhdlEndArchitectureName;
import com.staremax.vhdlplug.reference.VhdlReference;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VhdlEndArchitectureNameReference extends VhdlReference {

    public VhdlEndArchitectureNameReference(@NotNull VhdlEndArchitectureName element) {
        super(element);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        VhdlArchitectureBody parent = (VhdlArchitectureBody) myElement.getParent();
        if (myElement.getText().equalsIgnoreCase(parent.getArchitectureNameString())) {
            return parent;
        } else {
            return null;
        }
    }

    @NotNull
    @Override
    public Object[] getVariants() {
        VhdlArchitectureBody parent = (VhdlArchitectureBody) myElement.getParent();
        return new Object[]{parent.getArchitectureNameString()};
    }
}
