package com.staremax.vhdlplug.reference.impl;

import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.psi.VhdlEndEntityName;
import com.staremax.vhdlplug.psi.VhdlEntityDeclaration;
import com.staremax.vhdlplug.reference.VhdlReference;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VhdlEndEntityNameReference extends VhdlReference {

    public VhdlEndEntityNameReference(@NotNull VhdlEndEntityName element) {
        super(element);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        VhdlEntityDeclaration parent = (VhdlEntityDeclaration) this.myElement.getParent();
        if (myElement.getText().equalsIgnoreCase(parent.getEntityNameString())) {
            return parent;
        } else {
            return null;
        }
    }

    @NotNull
    @Override
    public Object[] getVariants() {
        VhdlEntityDeclaration parent = (VhdlEntityDeclaration) myElement.getParent();
        return new Object[]{parent.getEntityNameString()};
    }
}
