package com.staremax.vhdlplug.reference.impl;

import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.psi.VhdlSimpleName;
import com.staremax.vhdlplug.reference.VhdlReference;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VhdlSimpleNameReference extends VhdlReference {

    public VhdlSimpleNameReference(@NotNull VhdlSimpleName element) {
        super(element);
    }

    @Nullable
    @Override
    public PsiElement resolve() {
        // Stub
        return null;
    }
}
