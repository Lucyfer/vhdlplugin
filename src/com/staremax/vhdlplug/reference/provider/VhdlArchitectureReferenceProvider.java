package com.staremax.vhdlplug.reference.provider;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFileSystemItem;
import com.intellij.psi.PsiReference;
import com.intellij.psi.PsiReferenceProvider;
import com.intellij.util.ProcessingContext;
import com.staremax.vhdlplug.psi.VhdlArchitectureName;
import com.staremax.vhdlplug.psi.VhdlEndArchitectureName;
import com.staremax.vhdlplug.reference.impl.VhdlArchitectureNameReference;
import com.staremax.vhdlplug.reference.impl.VhdlEndArchitectureNameReference;
import org.jetbrains.annotations.NotNull;

public class VhdlArchitectureReferenceProvider extends PsiReferenceProvider {

    @Override
    public boolean acceptsTarget(@NotNull PsiElement target) {
        return target instanceof PsiFileSystemItem;
    }

    @NotNull
    @Override
    public PsiReference[] getReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
        if (element instanceof VhdlArchitectureName) {
            return new PsiReference[]{new VhdlArchitectureNameReference((VhdlArchitectureName) element)};
        } else if (element instanceof VhdlEndArchitectureName) {
            return new PsiReference[]{new VhdlEndArchitectureNameReference((VhdlEndArchitectureName) element)};
        } else return PsiReference.EMPTY_ARRAY;
    }
}
