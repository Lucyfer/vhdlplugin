package com.staremax.vhdlplug.reference.provider;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFileSystemItem;
import com.intellij.psi.PsiReference;
import com.intellij.psi.PsiReferenceProvider;
import com.intellij.util.ProcessingContext;
import com.staremax.vhdlplug.psi.VhdlEndSimpleName;
import com.staremax.vhdlplug.psi.VhdlPackageBody;
import com.staremax.vhdlplug.psi.VhdlPackageDeclaration;
import com.staremax.vhdlplug.psi.VhdlSimpleName;
import com.staremax.vhdlplug.reference.impl.VhdlEndSimpleNameReference;
import com.staremax.vhdlplug.reference.impl.VhdlPackageNameReference;
import com.staremax.vhdlplug.reference.impl.VhdlSimpleNameReference;
import org.jetbrains.annotations.NotNull;

public class VhdlSimpleNameReferenceProvider extends PsiReferenceProvider {

    @Override
    public boolean acceptsTarget(@NotNull PsiElement target) {
        return target instanceof PsiFileSystemItem;
    }

    @NotNull
    @Override
    public PsiReference[] getReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
        return getReferences(element);
    }

    @NotNull
    public static PsiReference[] getReferences(@NotNull PsiElement element) {
        if (element instanceof VhdlSimpleName || element instanceof VhdlEndSimpleName) {
            final PsiElement parent = element.getParent();

            if (parent instanceof VhdlPackageDeclaration || parent instanceof VhdlPackageBody) {
                return new PsiReference[]{new VhdlPackageNameReference(element)};
            } else {
                return new PsiReference[]{
                        (element instanceof VhdlSimpleName) ?
                                new VhdlSimpleNameReference((VhdlSimpleName) element) :
                                new VhdlEndSimpleNameReference((VhdlEndSimpleName) element)};
            }
        }
        return PsiReference.EMPTY_ARRAY;
    }
}
