package com.staremax.vhdlplug.reference.provider;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFileSystemItem;
import com.intellij.psi.PsiReference;
import com.intellij.psi.PsiReferenceProvider;
import com.intellij.util.ProcessingContext;
import com.staremax.vhdlplug.psi.VhdlEndEntityName;
import com.staremax.vhdlplug.psi.VhdlEntityName;
import com.staremax.vhdlplug.reference.impl.VhdlEndEntityNameReference;
import com.staremax.vhdlplug.reference.impl.VhdlEntityNameReference;
import org.jetbrains.annotations.NotNull;

public class VhdlEntityReferenceProvider extends PsiReferenceProvider {

    @Override
    public boolean acceptsTarget(@NotNull PsiElement target) {
        return target instanceof PsiFileSystemItem;
    }

    @NotNull
    @Override
    public PsiReference[] getReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context) {
        return getReferences(element);
    }

    @NotNull
    public static PsiReference[] getReferences(@NotNull PsiElement element) {
        if (element instanceof VhdlEndEntityName) {
            return new PsiReference[]{new VhdlEndEntityNameReference((VhdlEndEntityName) element)};
        } else if (element instanceof VhdlEntityName) {
            return new PsiReference[]{new VhdlEntityNameReference((VhdlEntityName) element)};
        } else return PsiReference.EMPTY_ARRAY;
    }
}
