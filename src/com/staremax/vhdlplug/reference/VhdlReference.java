package com.staremax.vhdlplug.reference;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiPolyVariantReference;
import com.intellij.psi.PsiReference;
import com.intellij.psi.ResolveResult;
import com.intellij.util.IncorrectOperationException;
import com.staremax.vhdlplug.psi.VhdlNamedElement;
import org.jetbrains.annotations.NotNull;

abstract public class VhdlReference implements PsiPolyVariantReference {

    protected final PsiElement myElement;

    public VhdlReference(@NotNull PsiElement element) {
        this.myElement = element;
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean incomplete) {
        return ResolveResult.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public Object[] getVariants() {
        return new Object[0];
    }

    @Override
    public boolean isReferenceTo(@NotNull PsiElement element) {
        PsiElement myResolved = resolve();
        PsiReference ref = element.getReference();
        PsiElement resolved = ref != null ? ref.resolve() : null;
        if (resolved == null) {
            resolved = element;
        }
        return myResolved != null && resolved.equals(myResolved);
    }

    @Override
    public boolean isSoft() {
        return false;
    }

    @Override
    public PsiElement bindToElement(@NotNull PsiElement element) throws IncorrectOperationException {
        return myElement;
    }

    @Override
    public PsiElement getElement() {
        return myElement;
    }

    @Override
    public TextRange getRangeInElement() {
        return new TextRange(0, myElement.getText().length());
    }

    @NotNull
    @Override
    public String getCanonicalText() {
        return myElement.getText();
    }

    @Override
    public PsiElement handleElementRename(@NotNull String newName) throws IncorrectOperationException {
        if (myElement instanceof VhdlNamedElement) {
            return ((VhdlNamedElement) myElement).setName(newName);
        }
        throw new IncorrectOperationException();
    }
}
