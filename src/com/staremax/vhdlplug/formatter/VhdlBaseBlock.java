package com.staremax.vhdlplug.formatter;

import com.intellij.formatting.*;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiComment;
import com.intellij.psi.PsiErrorElement;
import com.intellij.psi.PsiWhiteSpace;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.staremax.vhdlplug.formatter.processor.VhdlSpacingProcessor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * Describes a single block in the FormattingModel.
 */
public class VhdlBaseBlock implements Block {

    protected final ASTNode myNode;
    protected final Alignment myAlignment;
    protected final Indent myIndent;
    protected final Wrap myWrap;
    protected final CodeStyleSettings mySettings;
    protected final VhdlSpacingProcessor mySpacingProcessor;
    protected List<Block> mySubBlocks;

    public VhdlBaseBlock(Alignment myAlignment, ASTNode myNode, Indent myIndent,
                         Wrap myWrap, CodeStyleSettings mySettings, VhdlSpacingProcessor spacingProcessor) {
        this.myAlignment = myAlignment;
        this.myNode = myNode;
        this.myIndent = myIndent;
        this.myWrap = myWrap;
        this.mySettings = mySettings;
        this.mySpacingProcessor = spacingProcessor;
    }

    @Nullable
    @Override
    public Alignment getAlignment() {
        return myAlignment;
    }

    @NotNull
    @Override
    public TextRange getTextRange() {
        return myNode.getTextRange();
    }

    @NotNull
    @Override
    public List<Block> getSubBlocks() {
        if (mySubBlocks == null) {
            mySubBlocks = VhdlBlockGenerator.generateSubBlocks(this);
        }
        return mySubBlocks;
    }

    @Nullable
    @Override
    public Wrap getWrap() {
        return myWrap;
    }

    @Nullable
    @Override
    public Indent getIndent() {
        return myIndent;
    }

    public ASTNode getNode() {
        return myNode;
    }

    public CodeStyleSettings getSettings() {
        return mySettings;
    }

    public VhdlSpacingProcessor getSpacingProcessor() {
        return mySpacingProcessor;
    }

    @Nullable
    @Override
    public Spacing getSpacing(@Nullable Block block1, @NotNull Block block2) {
        return mySpacingProcessor.getSpacing(this, block1, block2);
    }

    @NotNull
    @Override
    public ChildAttributes getChildAttributes(int i) {
        return new ChildAttributes(Indent.getNormalIndent(), myAlignment);
    }

    @Override
    public boolean isIncomplete() {
        return isIncomplete(myNode);
    }

    private boolean isIncomplete(@NotNull ASTNode node) {
        ASTNode lastChild = node.getLastChildNode();
        while (lastChild != null &&
                (lastChild.getPsi() instanceof PsiWhiteSpace || lastChild.getPsi() instanceof PsiComment)) {
            lastChild = lastChild.getTreePrev();
        }
        return lastChild != null && (lastChild.getPsi() instanceof PsiErrorElement || isIncomplete(lastChild));
    }

    @Override
    public boolean isLeaf() {
        return myNode.getFirstChildNode() == null;
    }
}
