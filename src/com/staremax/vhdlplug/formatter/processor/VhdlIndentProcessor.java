package com.staremax.vhdlplug.formatter.processor;

import com.intellij.formatting.Indent;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.VhdlFile;
import com.staremax.vhdlplug.formatter.VhdlBaseBlock;
import com.staremax.vhdlplug.psi.*;

/**
 * Defines indentation of the blocks.
 */
public class VhdlIndentProcessor {
    public static Indent getChildIndent(VhdlBaseBlock parent, ASTNode prevChildNode, ASTNode childNode) {
        ASTNode astNode = parent.getNode();
        final PsiElement psiParent = astNode.getPsi();
        final PsiElement psiChild = childNode.getPsi();

        // for VHDL file
        if (psiParent instanceof VhdlFile) {
            return Indent.getAbsoluteNoneIndent();
        }
        if (psiParent instanceof VhdlDesignUnit) {
            return Indent.getAbsoluteNoneIndent();
        }
        // for names in the logical list
        if (psiParent instanceof VhdlLogicalNameList) {
            return Indent.getContinuationIndent(true);
        }
        // for names in the selected list
        if (psiParent instanceof VhdlSelectedNameList) {
            return Indent.getNoneIndent();
        }
        // for generic and port clauses in the entity header
        if (psiParent instanceof VhdlGenericClause || psiParent instanceof VhdlPortClause) {
            return Indent.getNormalIndent(true);
        }
        // for param list
        if (psiParent instanceof VhdlInterfaceList) {
            return Indent.getContinuationIndent(true);
        }
        // for declaration items
        if (psiParent instanceof VhdlPackageDeclarativeItem) {
            return Indent.getNormalIndent(true);
        }
        // for package body items
        if (psiParent instanceof VhdlPackageBodyDeclarativeItem) {
            return Indent.getNormalIndent(true);
        }
        // for identifier list
        if (psiParent instanceof VhdlIdentifierList) {
            return Indent.getNoneIndent();
        }
        // for subprogram actual param list
        if (psiParent instanceof VhdlAssociationList) {
            return Indent.getNoneIndent();
        }
        // for sensitivity list in process or wait stmt
        if (psiParent instanceof VhdlSensitivityList) {
            return Indent.getNoneIndent();
        }
        // for declarative items block
        if (psiParent instanceof VhdlBlockDeclarativeItem) {
            return Indent.getNormalIndent(true);
        }
        // indentation for statements of the architecture, block, generate body
        if (psiParent instanceof VhdlConcurrentStatement) {
            return Indent.getNormalIndent(true);
        }
        // for subprogram declarations
        if (psiParent instanceof VhdlSubprogramDeclarativeItem) {
            return Indent.getNormalIndent(true);
        }
        // for subprogram statements
        if (psiParent instanceof VhdlSequentialStatement) {
            return Indent.getNormalIndent(true);
        }
        // for process declarations
        if (psiParent instanceof VhdlProcessDeclarativeItem) {
            return Indent.getNormalIndent(true);
        }
        // for map aspects
        if (psiParent instanceof VhdlGenericMapAspect || psiParent instanceof VhdlPortMapAspect) {
            return Indent.getNormalIndent(true);
        }
        // for assert
        if (psiParent instanceof VhdlReportExpr || psiParent instanceof VhdlSeverityExpr) {
            return Indent.getNormalIndent(true);
        }
        // for target and waveforms in the selected signal
        if (psiParent instanceof VhdlSelectedSignalAssignment) {
            if (psiChild instanceof VhdlTarget || psiChild instanceof VhdlSelectedWaveforms) {
                return Indent.getNormalIndent(true);
            }
        }
        // for waveforms in the conditional signal
        if (psiParent instanceof VhdlConditionalSignalAssignment) {
            if (psiChild instanceof VhdlConditionalWaveforms) {
                return Indent.getNormalIndent(true);
            }
        }
        // for case inner statements
        if (psiParent instanceof VhdlCaseStatementAlternative) {
            return Indent.getNormalIndent(true);
        }
        return Indent.getNoneIndent();
    }
}
