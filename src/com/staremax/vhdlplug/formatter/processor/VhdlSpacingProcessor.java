package com.staremax.vhdlplug.formatter.processor;

import com.intellij.formatting.Block;
import com.intellij.formatting.Spacing;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.intellij.psi.tree.IElementType;
import com.staremax.vhdlplug.formatter.VhdlBaseBlock;
import com.staremax.vhdlplug.lexer.VhdlTokenTypesExtension;
import com.staremax.vhdlplug.psi.VhdlCaseStatementAlternative;
import com.staremax.vhdlplug.psi.VhdlSign;
import com.staremax.vhdlplug.psi.VhdlTypes;

/**
 * Defines a spacing between a blocks.
 */
public class VhdlSpacingProcessor {
    private static final Spacing NO_SPACING = Spacing.createSpacing(0, 0, 0, false, 0);
    private static final Spacing NO_SPACING_WITH_NEWLINE = Spacing.createSpacing(0, 0, 0, true, 1);
    private static final Spacing MANDATORY_NEWLINE = Spacing.createSpacing(0, 0, 1, true, 100);
    private static final Spacing COMMON_SPACING = Spacing.createSpacing(1, 1, 0, true, 100);
    private static final Spacing NO_NEWLINE = Spacing.createSpacing(1, 1, 0, false, 0);

    private final CodeStyleSettings settings;

    public VhdlSpacingProcessor(CodeStyleSettings settings) {
        this.settings = settings;
    }

    public Spacing getSpacing(Block parent, Block child1, Block child2) {
        if (!(parent instanceof VhdlBaseBlock) || !(child1 instanceof VhdlBaseBlock)
                || !(child2 instanceof VhdlBaseBlock)) {
            return null;
        }

        final VhdlBaseBlock parentBlock = (VhdlBaseBlock) parent;
        final VhdlBaseBlock block1 = (VhdlBaseBlock) child1;
        final VhdlBaseBlock block2 = (VhdlBaseBlock) child2;

        final ASTNode parentNode = parentBlock.getNode();
        final ASTNode node1 = block1.getNode();
        final ASTNode node2 = block2.getNode();

        final IElementType parentType = parentNode.getElementType();
        final IElementType type1 = node1.getElementType();
        final IElementType type2 = node2.getElementType();

        final Spacing psiSpacing = psiBasedSpacing(parentNode.getPsi(), node1.getPsi(), node2.getPsi());
        if (psiSpacing != null) {
            return psiSpacing;
        }

        if (VhdlTokenTypesExtension.COMMENTS.contains(type1) ||
                VhdlTokenTypesExtension.COMMENTS.contains(type2)) {
            return null;
        }
        if (safeEqual(parentType, VhdlTypes.CONDITIONAL_WAVEFORMS)) {
            if (safeEqual(type1, VhdlTypes.ELSE) || safeEqual(type2, VhdlTypes.ELSE)) {
                return COMMON_SPACING;
            }
        }
        if (safeEqual(type1, VhdlTypes.ASSIGN) || safeEqual(type2, VhdlTypes.ASSIGN)) {
            return settings.SPACE_AROUND_ASSIGNMENT_OPERATORS ? COMMON_SPACING : NO_SPACING;
        }
        if (safeEqual(type1, VhdlTypes.DOT) || safeEqual(type2, VhdlTypes.DOT)) {
            return NO_SPACING_WITH_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.SINGLE_QUOTE) || safeEqual(type2, VhdlTypes.SINGLE_QUOTE)) {
            return NO_SPACING_WITH_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.COMMA)) {
            return settings.SPACE_AFTER_COMMA ? COMMON_SPACING : NO_SPACING_WITH_NEWLINE;
        }
        if (safeEqual(type2, VhdlTypes.COMMA)) {
            return settings.SPACE_BEFORE_COMMA ? COMMON_SPACING : NO_SPACING_WITH_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.SEMICOLON)) {
            return settings.SPACE_AFTER_SEMICOLON ? COMMON_SPACING : NO_SPACING_WITH_NEWLINE;
        }
        if (safeEqual(type2, VhdlTypes.SEMICOLON)) {
            return settings.SPACE_BEFORE_SEMICOLON ? COMMON_SPACING : NO_SPACING_WITH_NEWLINE;
        }
        if (safeEqual(type2, VhdlTypes.COLON)) {
            return settings.SPACE_BEFORE_COLON ? COMMON_SPACING : NO_SPACING_WITH_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.COLON)) {
            return settings.SPACE_AFTER_COLON ? COMMON_SPACING : NO_SPACING_WITH_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.LPAREN)) {
            return settings.SPACE_WITHIN_PARENTHESES ? COMMON_SPACING : NO_SPACING;
        }
        if (safeEqual(type2, VhdlTypes.RPAREN)) {
            return settings.SPACE_WITHIN_PARENTHESES ? COMMON_SPACING : NO_SPACING;
        }
        if (safeEqual(type1, VhdlTypes.BEGIN)) {
            return MANDATORY_NEWLINE;
        }
        if (safeEqual(type2, VhdlTypes.BEGIN)) {
            return MANDATORY_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.IS)) {
            return safeEqual(type2, VhdlTypes.END) ? NO_NEWLINE : COMMON_SPACING;
        }
        if (safeEqual(type2, VhdlTypes.END)) {
            return safeEqual(type1, VhdlTypes.BEGIN) ? NO_NEWLINE : MANDATORY_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.END)) {
            return NO_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.LIBRARY) && safeEqual(type2, VhdlTypes.LOGICAL_NAME_LIST)) {
            return NO_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.USE) && safeEqual(type2, VhdlTypes.SELECTED_NAME_LIST)) {
            return NO_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.IF) || safeEqual(type1, VhdlTypes.ELSIF)) {
            return NO_NEWLINE;
        }
        if (safeEqual(type2, VhdlTypes.ELSIF)) {
            return MANDATORY_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.THEN)) {
            return MANDATORY_NEWLINE;
        }
        if (safeEqual(type2, VhdlTypes.THEN)) {
            return NO_NEWLINE;
        }
        if (safeEqual(type2, VhdlTypes.ELSE) || safeEqual(type1, VhdlTypes.ELSE)) {
            return MANDATORY_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.ARRAY)) {
            return NO_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.RANGE)) {
            return NO_NEWLINE;
        }
        if (safeEqual(type2, VhdlTypes.LOOP)) {
            return NO_NEWLINE;
        }
        if (safeEqual(type1, VhdlTypes.LOOP)) {
            return MANDATORY_NEWLINE;
        }
        return COMMON_SPACING;
    }

    private boolean safeEqual(Object a, Object b) {
        return a != null && a.equals(b);
    }

    private Spacing psiBasedSpacing(PsiElement parent, PsiElement psi1, PsiElement psi2) {
        if (psi1 instanceof VhdlSign) {
            return NO_SPACING;
        }
        if (psi2 instanceof VhdlCaseStatementAlternative) {
            return MANDATORY_NEWLINE;
        }
        return null;
    }
}
