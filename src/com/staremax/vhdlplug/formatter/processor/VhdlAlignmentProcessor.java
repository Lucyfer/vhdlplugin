package com.staremax.vhdlplug.formatter.processor;

import com.intellij.formatting.Alignment;
import com.intellij.formatting.Block;
import com.intellij.psi.PsiElement;
import com.intellij.psi.tree.IElementType;
import com.staremax.vhdlplug.formatter.codeStyle.VhdlCodeStyleSettings;
import com.staremax.vhdlplug.psi.*;
import org.jetbrains.annotations.NotNull;

/**
 * Defines an alignment for a blocks.
 */
public class VhdlAlignmentProcessor {

    @NotNull
    public static Alignment getChildAlignment(Block block, PsiElement parentPsi,
                                              PsiElement childPsi, VhdlCodeStyleSettings settings) {
        if (parentPsi instanceof VhdlLogicalNameList) {
            IElementType type = childPsi.getNode().getElementType();
            if (childPsi instanceof VhdlSimpleName || type.equals(VhdlTypes.COMMA)) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlSelectedNameList) {
            if (childPsi instanceof VhdlSelectedName) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlAssociationList) {
            if (childPsi instanceof VhdlAssociationElement) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlSensitivityList) {
            if (childPsi instanceof VhdlGeneralName) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlEntityDeclaration) {
            IElementType type = childPsi.getNode().getElementType();
            if (type.equals(VhdlTypes.ENTITY) || type.equals(VhdlTypes.END)) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlEntityHeader) {
            if (childPsi instanceof VhdlGenericClause || childPsi instanceof VhdlPortClause) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlGenericClause || parentPsi instanceof VhdlPortClause) {
            return Alignment.createChildAlignment(block.getAlignment());
        } else if (parentPsi instanceof VhdlInterfaceList) {
            return Alignment.createChildAlignment(block.getAlignment());
        } else if (parentPsi instanceof VhdlPackageDeclaration) {
            IElementType type = childPsi.getNode().getElementType();
            if (type.equals(VhdlTypes.PACKAGE) || type.equals(VhdlTypes.END)) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlPackageDeclarativePart) {
            if (childPsi instanceof VhdlPackageDeclarativeItem) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlIdentifierList) {
            if (childPsi instanceof VhdlSimpleName) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlArchitectureBody) {
            IElementType type = childPsi.getNode().getElementType();
            if (type.equals(VhdlTypes.ARCHITECTURE) || type.equals(VhdlTypes.BEGIN) || type.equals(VhdlTypes.END)) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlArchitectureDeclarativePart) {
            if (childPsi instanceof VhdlBlockDeclarativeItem) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlArchitectureStatementPart) {
            if (childPsi instanceof VhdlConcurrentStatement) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlSubprogramBody) {
            IElementType type = childPsi.getNode().getElementType();
            if (childPsi instanceof VhdlSubprogramSpecification || type.equals(VhdlTypes.BEGIN) || type.equals(VhdlTypes.END)) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlSubprogramDeclarativePart) {
            if (childPsi instanceof VhdlSubprogramDeclarativeItem) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlSubprogramStatementPart) {
            if (childPsi instanceof VhdlSequentialStatement) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlBlockStatement) {
            IElementType type = childPsi.getNode().getElementType();
            if (type.equals(VhdlTypes.BLOCK) || type.equals(VhdlTypes.BEGIN) || type.equals(VhdlTypes.END)) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlProcessStatement) {
            IElementType type = childPsi.getNode().getElementType();
            if (type.equals(VhdlTypes.PROCESS) || type.equals(VhdlTypes.POSTPONED)
                    || type.equals(VhdlTypes.BEGIN) || type.equals(VhdlTypes.END)) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlIfStatement) {
            IElementType type = childPsi.getNode().getElementType();
            if (type.equals(VhdlTypes.IF) || type.equals(VhdlTypes.ELSIF) || type.equals(VhdlTypes.THEN)
                    || type.equals(VhdlTypes.ELSE) || type.equals(VhdlTypes.END)) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlProcessDeclarativePart) {
            if (childPsi instanceof VhdlProcessDeclarativeItem) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlIndexSubtypeDefinitionList) {
            if (childPsi instanceof VhdlIndexSubtypeDefinition) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlDiscreteRangeList) {
            if (childPsi instanceof VhdlDiscreteRange) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlComponentDeclaration) {
            IElementType type = childPsi.getNode().getElementType();
            if (type.equals(VhdlTypes.COMPONENT) || type.equals(VhdlTypes.END)) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlEntityNameList) {
            if (childPsi instanceof VhdlEntityDesignator) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlSignalList) {
            if (childPsi instanceof VhdlGeneralName) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlGroupConstituentList) {
            if (childPsi instanceof VhdlGroupConstituent) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlBindingIndication) {
            IElementType type = childPsi.getNode().getElementType();
            if (type.equals(VhdlTypes.USE) || childPsi instanceof VhdlGenericMapAspect
                    || childPsi instanceof VhdlPortMapAspect) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlChoices) {
            if (childPsi instanceof VhdlChoice) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlElementAssociationList) {
            if (childPsi instanceof VhdlElementAssociation) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlWaveform) {
            if (childPsi instanceof VhdlWaveformElement) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlCaseStatement) {
            IElementType type = childPsi.getNode().getElementType();
            if (type.equals(VhdlTypes.CASE) || type.equals(VhdlTypes.END)) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        } else if (parentPsi instanceof VhdlLoopStatement) {
            IElementType type = childPsi.getNode().getElementType();
            if (type.equals(VhdlTypes.LOOP) || type.equals(VhdlTypes.END)) {
                return Alignment.createChildAlignment(block.getAlignment());
            }
        }
        return Alignment.createAlignment(true);
    }
}
