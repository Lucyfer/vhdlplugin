package com.staremax.vhdlplug.formatter;

import com.intellij.formatting.*;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.staremax.vhdlplug.formatter.processor.VhdlSpacingProcessor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Creates a formatting model.
 */
public class VhdlFormattingModelBuilder implements FormattingModelBuilder {
    @NotNull
    @Override
    public FormattingModel createModel(PsiElement element, CodeStyleSettings settings) {
        VhdlSpacingProcessor spacingProcessor = new VhdlSpacingProcessor(settings);
        return FormattingModelProvider.createFormattingModelForPsiFile(
                element.getContainingFile(),
                new VhdlBaseBlock(Alignment.createAlignment(), element.getNode(), Indent.getAbsoluteNoneIndent(),
                        Wrap.createWrap(WrapType.NORMAL, false), settings, spacingProcessor),
                settings);
    }

    @Nullable
    @Override
    public TextRange getRangeAffectingIndent(PsiFile file, int offset, ASTNode elementAtOffset) {
        return null;
    }
}
