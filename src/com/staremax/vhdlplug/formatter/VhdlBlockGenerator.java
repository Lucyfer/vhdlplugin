package com.staremax.vhdlplug.formatter;

import com.intellij.formatting.Alignment;
import com.intellij.formatting.Block;
import com.intellij.formatting.Indent;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiWhiteSpace;
import com.staremax.vhdlplug.formatter.codeStyle.VhdlCodeStyleSettings;
import com.staremax.vhdlplug.formatter.processor.VhdlAlignmentProcessor;
import com.staremax.vhdlplug.formatter.processor.VhdlIndentProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * Generates a blocks structure.
 */
public class VhdlBlockGenerator {
    public static List<Block> generateSubBlocks(VhdlBaseBlock block) {
        final ArrayList<Block> subBlocks = new ArrayList<Block>();
        final PsiElement blockPsi = block.getNode().getPsi();
        final ASTNode children[] = block.getNode().getChildren(null);
        final VhdlCodeStyleSettings settings = block.getSettings().getCustomSettings(VhdlCodeStyleSettings.class);

        ASTNode prevChildNode = null;

        for (final ASTNode childNode : children) {
            if (canBeCorrectBlock(childNode)) {
                final PsiElement childPsi = childNode.getPsi();
                final Indent indent = VhdlIndentProcessor.getChildIndent(block, prevChildNode, childNode);
                final Alignment childAlignment = VhdlAlignmentProcessor.getChildAlignment(block, blockPsi,
                        childPsi, settings);
                subBlocks.add(new VhdlBaseBlock(childAlignment, childNode, indent, block.getWrap(),
                        block.getSettings(), block.getSpacingProcessor()));
                prevChildNode = childNode;
            }
        }
        return subBlocks;
    }

    private static boolean canBeCorrectBlock(final ASTNode node) {
        return !(node.getPsi() instanceof PsiWhiteSpace)
                && (node.getText().trim().length() > 0);
    }
}
