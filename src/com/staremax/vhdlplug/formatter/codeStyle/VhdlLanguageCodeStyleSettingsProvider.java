package com.staremax.vhdlplug.formatter.codeStyle;

import com.intellij.lang.Language;
import com.intellij.psi.codeStyle.CodeStyleSettingsCustomizable;
import com.intellij.psi.codeStyle.LanguageCodeStyleSettingsProvider;
import com.staremax.vhdlplug.VhdlLanguage;
import org.jetbrains.annotations.NotNull;

/**
 * Code style settings panel.
 */
public class VhdlLanguageCodeStyleSettingsProvider extends LanguageCodeStyleSettingsProvider {

    @NotNull
    @Override
    public Language getLanguage() {
        return VhdlLanguage.INSTANCE;
    }

    @Override
    public void customizeSettings(@NotNull CodeStyleSettingsCustomizable consumer, @NotNull SettingsType settingsType) {
        if (settingsType == SettingsType.SPACING_SETTINGS) {
            consumer.showStandardOptions(
                    "SPACE_AROUND_ASSIGNMENT_OPERATORS",
                    "SPACE_AFTER_COMMA",
                    "SPACE_BEFORE_COMMA",
                    "SPACE_AFTER_COLON",
                    "SPACE_BEFORE_COLON",
                    "SPACE_AFTER_SEMICOLON",
                    "SPACE_BEFORE_SEMICOLON",
                    "SPACE_WITHIN_PARENTHESES");
        }
    }

    @Override
    public String getCodeSample(@NotNull SettingsType settingsType) {
        return "-- comment line\n" +
                "TYPE bit IS ('0', '1');\n" +
                "\n" +
                "ENTITY change_state IS\n" +
                "   PORT(SIGNAL x: IN  real;\n" +
                "        SIGNAL y: OUT real);\n" +
                "END ENTITY change_state;\n" +
                "\n" +
                "ARCHITECTURE behavior OF change_state IS\n" +
                "   SIGNAL bitArrayName : bit_vector (3 downto 0):=\"0000\";\n" +
                "BEGIN\n" +
                "   p: PROCESS(x)\n" +
                "   BEGIN\n" +
                "       IF x > 5.0 THEN y <= 0.1;\n" +
                "       ELSE IF x < 0 THEN y <= -5.0;\n" +
                "       END IF; END IF;\n" +
                "   END PROCESS p;\n" +
                "   bitArrayName <= \"1111\";\n" +
                "END ARCHITECTURE behavior;";
    }
}
