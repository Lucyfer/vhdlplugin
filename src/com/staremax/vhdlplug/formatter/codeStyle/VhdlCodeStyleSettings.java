package com.staremax.vhdlplug.formatter.codeStyle;

import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.intellij.psi.codeStyle.CustomCodeStyleSettings;

public class VhdlCodeStyleSettings extends CustomCodeStyleSettings {
    public VhdlCodeStyleSettings(CodeStyleSettings container) {
        super("VhdlCodeStyleSettings", container);
    }
}
