package com.staremax.vhdlplug.components;

import com.intellij.openapi.components.ApplicationComponent;
import com.staremax.vhdlplug.bundle.VhdlBundle;
import org.jetbrains.annotations.NotNull;

public class VhdlPluginRegistration implements ApplicationComponent {

    @Override
    public void disposeComponent() {
        // do nothing
    }

    @Override
    public void initComponent() {
        // do nothing
    }


    @NotNull
    @Override
    public String getComponentName() {
        return VhdlBundle.message("vhdl.plugin.name");
    }
}
