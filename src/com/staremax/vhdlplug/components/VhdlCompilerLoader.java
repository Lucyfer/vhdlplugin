package com.staremax.vhdlplug.components;

import com.intellij.compiler.CompilerWorkspaceConfiguration;
import com.intellij.openapi.compiler.CompilerManager;
import com.intellij.openapi.components.AbstractProjectComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.project.Project;
import com.staremax.vhdlplug.VhdlFileType;
import com.staremax.vhdlplug.compiler.GhdlCompiler;
import com.staremax.vhdlplug.compiler.GhdlMakefileCompiler;
import com.staremax.vhdlplug.ide.module.VhdlModuleType;
import com.staremax.vhdlplug.settings.VhdlSettings;
import com.staremax.vhdlplug.settings.VhdlState;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Current compiler configuration
 */
public class VhdlCompilerLoader extends AbstractProjectComponent {

    private boolean myProjectUsesVhdl;
    private boolean myPreviousExternalCompilerSetting;
    private CompilerWorkspaceConfiguration myConfiguration;

    public VhdlCompilerLoader(Project project) {
        super(project);

        this.myProjectUsesVhdl = false;
        this.myPreviousExternalCompilerSetting = false;
    }

    @NotNull
    @Override
    public String getComponentName() {
        return "VhdlCompilerLoader";
    }

    @Override
    public void projectOpened() {
        myConfiguration = CompilerWorkspaceConfiguration.getInstance(myProject);
        ModuleManager moduleManager = ModuleManager.getInstance(myProject);

        for (Module module : moduleManager.getModules()) {
            if (ModuleType.get(module).equals(VhdlModuleType.getInstance()))
                myProjectUsesVhdl = true;
        }

        if (myProjectUsesVhdl) {
            myPreviousExternalCompilerSetting = myConfiguration.USE_COMPILE_SERVER;
            myConfiguration.USE_COMPILE_SERVER = false;
        }

        CompilerManager compilerManager = CompilerManager.getInstance(myProject);

        for (GhdlCompiler compiler : getCompilerManager().getCompilers(GhdlCompiler.class)) {
            getCompilerManager().removeCompiler(compiler);
        }

        for (GhdlMakefileCompiler compiler : getCompilerManager().getCompilers(GhdlMakefileCompiler.class)) {
            getCompilerManager().removeCompiler(compiler);
        }

        compilerManager.addCompilableFileType(VhdlFileType.INSTANCE);

        final VhdlState state = ServiceManager.getService(VhdlSettings.class).getState();
        final boolean useMakefile = (state != null ? state : VhdlSettings.getDefaults()).isUseMakefile();
        if (useMakefile) {
            compilerManager.addTranslatingCompiler(
                    new GhdlMakefileCompiler(myProject),
                    new HashSet<FileType>(Arrays.asList(VhdlFileType.INSTANCE)),
                    new HashSet<FileType>(Arrays.asList(FileType.EMPTY_ARRAY)));
        } else {
            compilerManager.addTranslatingCompiler(
                    new GhdlCompiler(myProject),
                    new HashSet<FileType>(Arrays.asList(VhdlFileType.INSTANCE)),
                    new HashSet<FileType>(Arrays.asList(FileType.EMPTY_ARRAY)));
        }
    }

    private CompilerManager getCompilerManager() {
        return CompilerManager.getInstance(myProject);
    }

    @Override
    public void projectClosed() {
        if (myProjectUsesVhdl) {
            myConfiguration.USE_COMPILE_SERVER = myPreviousExternalCompilerSetting;
        }
    }
}
