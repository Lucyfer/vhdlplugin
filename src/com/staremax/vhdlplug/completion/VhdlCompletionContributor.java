package com.staremax.vhdlplug.completion;

import com.intellij.codeInsight.completion.*;
import com.intellij.patterns.ElementPattern;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.patterns.PsiElementPattern;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiErrorElement;
import com.intellij.util.ProcessingContext;
import com.staremax.vhdlplug.VhdlFile;
import com.staremax.vhdlplug.completion.handlers.*;
import com.staremax.vhdlplug.psi.VhdlDesignUnit;
import org.jetbrains.annotations.NotNull;

/**
 * Autocompletion using context
 */
public class VhdlCompletionContributor extends CompletionContributor {

    private static final ElementPattern<? extends PsiElement> TOP_LEVEL_PARENTS = PlatformPatterns.or(
            PlatformPatterns.psiElement(VhdlFile.class),
            PlatformPatterns.psiElement(VhdlDesignUnit.class),
            PlatformPatterns.psiElement(PsiErrorElement.class).withParent(VhdlDesignUnit.class),
            PlatformPatterns.psiElement(PsiErrorElement.class).withParent(VhdlFile.class)
    );

    public static final PsiElementPattern.Capture<PsiElement>
            TOP_LEVEL_LEAF = PlatformPatterns.psiElement().withParent(TOP_LEVEL_PARENTS);

    public static final ElementPattern<? extends PsiElement> OTHER = PlatformPatterns.and(
            PlatformPatterns.not(TOP_LEVEL_LEAF)
    );

    public VhdlCompletionContributor() {
        extend(CompletionType.BASIC,
                TOP_LEVEL_LEAF,
                new CompletionProvider<CompletionParameters>() {
                    @Override
                    protected void addCompletions(@NotNull CompletionParameters parameters,
                                                  ProcessingContext context,
                                                  @NotNull CompletionResultSet resultSet) {
                        resultSet.addElement(VhdlCompletionUtil.keyword("library", new AfterLibraryInsertHandler()));
                        resultSet.addElement(VhdlCompletionUtil.keyword("use", new AfterUseInsertHandler()));
                        resultSet.addElement(VhdlCompletionUtil.keyword("entity", new AfterEntityInsertHandler()));
                        resultSet.addElement(VhdlCompletionUtil.keyword("architecture", new AfterArchitectureInsertHandler()));
                        resultSet.addElement(VhdlCompletionUtil.keyword("package", new AfterPackageInsertHandler()));
                        resultSet.addElement(VhdlCompletionUtil.keyword("package body", new AfterPackageBodyInsertHandler()));
                    }
                });

        extend(CompletionType.BASIC,
                OTHER,
                new CompletionProvider<CompletionParameters>() {
                    @Override
                    protected void addCompletions(@NotNull CompletionParameters parameters,
                                                  ProcessingContext context,
                                                  @NotNull CompletionResultSet resultSet) {
                        String[] primaryTokenSet = new String[]{
                                "abs", "access", "after", "alias", "all", "and", "array",
                                "assert", "attribute", "begin", "block", "body", "buffer", "bus", "case", "component",
                                "configuration", "constant", "disconnect", "downto", "else", "elsif", "end",
                                "exit", "file", "for", "function", "generate", "group", "guarded", "if",
                                "impure", "in", "inertial", "inout", "is", "label", "linkage", "literal",
                                "loop", "map", "mod", "nand", "new", "next", "nor", "not", "null", "of", "on", "open",
                                "or", "others", "out", "postponed", "procedure", "process", "pure",
                                "range", "record", "register", "reject", "rem", "report", "return", "rol", "ror", "select",
                                "severity", "signal", "shared", "sla", "sll", "sra", "srl", "subtype", "then", "to",
                                "transport", "type", "unaffected", "units", "until", "variable", "wait", "when",
                                "while", "with", "xnor", "xor"
                        };
                        for (String s : primaryTokenSet) {
                            resultSet.addElement(VhdlCompletionUtil.keyword(s));
                        }
                        resultSet.addElement(VhdlCompletionUtil.keyword("generic", new AfterGenericInsertHandler()));
                        resultSet.addElement(VhdlCompletionUtil.keyword("port", new AfterPortInsertHandler()));
                    }
                });
    }
}
