package com.staremax.vhdlplug.completion;

import com.intellij.codeInsight.completion.InsertHandler;
import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.staremax.vhdlplug.completion.handlers.KeywordInsertHandler;
import org.jetbrains.annotations.Nullable;

/**
 * Utils for completion
 */
public class VhdlCompletionUtil {
    public static LookupElement keyword(String keyword) {
        return keyword(keyword, new KeywordInsertHandler());
    }

    /**
     * Create element in the completion list
     */
    public static LookupElement keyword(String keyword, @Nullable InsertHandler<LookupElement> handler) {
        return LookupElementBuilder.create(keyword)
                .bold()
                .withTypeText("keyword")
                .withInsertHandler(handler);
    }
}
