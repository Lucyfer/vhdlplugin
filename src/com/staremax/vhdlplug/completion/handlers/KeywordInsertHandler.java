package com.staremax.vhdlplug.completion.handlers;

import com.intellij.codeInsight.lookup.LookupElement;

public class KeywordInsertHandler extends VhdlInsertHandler<LookupElement> {

    @Override
    protected String getInsertionText() {
        return " ";
    }

    @Override
    protected boolean shouldPressEnter() {
        return false;
    }

    @Override
    protected int nextCaretPosition() {
        return 1;
    }
}
