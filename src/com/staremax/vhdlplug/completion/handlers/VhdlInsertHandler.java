package com.staremax.vhdlplug.completion.handlers;

import com.intellij.codeInsight.completion.InsertHandler;
import com.intellij.codeInsight.completion.InsertionContext;
import com.intellij.codeInsight.lookup.LookupElement;
import com.staremax.vhdlplug.utils.EditorUtil;

/**
 * Defines a callback invoking after completion
 * @param <T> lookup element
 */
public abstract class VhdlInsertHandler<T extends LookupElement> implements InsertHandler<T> {

    @Override
    public void handleInsert(InsertionContext context, T item) {
        int offset = context.getTailOffset();
        String text = getInsertionText();
        context.getDocument().insertString(offset, text);

        if (nextCaretPosition() != 0) {
            context.getEditor().getCaretModel().moveToOffset(offset + nextCaretPosition());
        }

        if (shouldReformat()) {
            EditorUtil.reformatPositions(context.getFile(), offset, offset + text.length());
        }

        if (shouldPressEnter()) {
            EditorUtil.pressEnter(context.getEditor());
        }
    }

    protected abstract String getInsertionText();

    protected abstract boolean shouldPressEnter();

    protected int nextCaretPosition() {
        return 1;
    }

    protected boolean shouldReformat() {
        return false;
    }
}
