package com.staremax.vhdlplug.completion.handlers;

public class AfterUseInsertHandler extends KeywordInsertHandler {

    @Override
    protected String getInsertionText() {
        return super.getInsertionText() + "IEEE.std_logic_1164.all;";
    }

    @Override
    protected boolean shouldPressEnter() {
        return true;
    }

    @Override
    protected int nextCaretPosition() {
        return super.getInsertionText().length() + 1;
    }

    @Override
    protected boolean shouldReformat() {
        return true;
    }
}
