package com.staremax.vhdlplug.completion.handlers;

public class AfterGenericInsertHandler extends KeywordInsertHandler {
    @Override
    protected String getInsertionText() {
        return super.getInsertionText() + "();";
    }

    @Override
    protected boolean shouldPressEnter() {
        return false;
    }

    @Override
    protected int nextCaretPosition() {
        return super.getInsertionText().length() + 1;
    }

    @Override
    protected boolean shouldReformat() {
        return false;
    }
}
