package com.staremax.vhdlplug.completion.handlers;

/**
 * Insert handler after architecture completion
 */
public class AfterArchitectureInsertHandler extends KeywordInsertHandler {
    @Override
    protected String getInsertionText() {
        return super.getInsertionText() + "new_arch of some_entity is\nbegin\nend;";
    }

    @Override
    protected boolean shouldPressEnter() {
        return true;
    }

    @Override
    protected int nextCaretPosition() {
        return super.getInsertionText().length() + 1;
    }

    @Override
    protected boolean shouldReformat() {
        return true;
    }
}
