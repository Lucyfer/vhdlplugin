package com.staremax.vhdlplug.completion.handlers;

public class AfterLibraryInsertHandler extends KeywordInsertHandler {

    @Override
    protected String getInsertionText() {
        return super.getInsertionText() + "IEEE;";
    }

    @Override
    protected boolean shouldPressEnter() {
        return true;
    }

    @Override
    protected int nextCaretPosition() {
        return super.getInsertionText().length() + 1;
    }

    @Override
    protected boolean shouldReformat() {
        return true;
    }
}
