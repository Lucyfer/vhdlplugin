package com.staremax.vhdlplug.completion.handlers;

public class AfterPackageBodyInsertHandler extends KeywordInsertHandler {
    @Override
    protected String getInsertionText() {
        return super.getInsertionText() + "new_package is\nend package body;";
    }

    @Override
    protected boolean shouldPressEnter() {
        return true;
    }

    @Override
    protected int nextCaretPosition() {
        return super.getInsertionText().length() + 1;
    }

    @Override
    protected boolean shouldReformat() {
        return false;
    }
}
