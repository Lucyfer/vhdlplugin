package com.staremax.vhdlplug.parser;

import com.intellij.lang.Language;
import com.intellij.psi.tree.IFileElementType;
import com.staremax.vhdlplug.VhdlLanguage;

public interface VhdlElementTypes {
    IFileElementType FILE = new IFileElementType(Language.<VhdlLanguage>findInstance(VhdlLanguage.class));
}
