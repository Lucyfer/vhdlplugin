package com.staremax.vhdlplug.parser;

import com.intellij.lang.ASTNode;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.FlexAdapter;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import com.staremax.vhdlplug.VhdlFile;
import com.staremax.vhdlplug.lexer.VhdlLexer;
import com.staremax.vhdlplug.lexer.VhdlTokenTypesExtension;
import com.staremax.vhdlplug.psi.VhdlTypes;
import org.jetbrains.annotations.NotNull;

import java.io.Reader;

public class VhdlParserDefinition implements ParserDefinition {

    @NotNull
    @Override
    public PsiElement createElement(ASTNode astNode) {
        return VhdlTypes.Factory.createElement(astNode);
    }

    @NotNull
    @Override
    public Lexer createLexer(Project project) {
        return new FlexAdapter(new VhdlLexer((Reader)null));
    }

    @Override
    public PsiParser createParser(Project project) {
        return new VhdlParser();
    }

    @Override
    public IFileElementType getFileNodeType() {
        return VhdlElementTypes.FILE;
    }

    @NotNull
    @Override
    public TokenSet getWhitespaceTokens() {
        return VhdlTokenTypesExtension.WHITESPACES;
    }

    @NotNull
    @Override
    public TokenSet getCommentTokens() {
        return VhdlTokenTypesExtension.COMMENTS;
    }

    @NotNull
    @Override
    public TokenSet getStringLiteralElements() {
        return VhdlTokenTypesExtension.STRING_LITERALS;
    }

    @Override
    public PsiFile createFile(FileViewProvider fileViewProvider) {
        return new VhdlFile(fileViewProvider);
    }

    @Override
    public SpaceRequirements spaceExistanceTypeBetweenTokens(ASTNode astNode, ASTNode astNode2) {
        return SpaceRequirements.MAY;
    }
}
