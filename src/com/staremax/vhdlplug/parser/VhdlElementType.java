package com.staremax.vhdlplug.parser;

import com.intellij.psi.tree.IElementType;
import com.staremax.vhdlplug.VhdlLanguage;
import org.jetbrains.annotations.NotNull;

public class VhdlElementType extends IElementType {
    public VhdlElementType(@NotNull String debugName) {
        super(debugName, VhdlLanguage.INSTANCE);
    }
}
