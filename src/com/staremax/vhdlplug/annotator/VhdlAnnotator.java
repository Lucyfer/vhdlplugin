package com.staremax.vhdlplug.annotator;

import com.intellij.codeInsight.intention.IntentionAction;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.staremax.vhdlplug.ide.intention.FixEndNameIntention;
import com.staremax.vhdlplug.psi.VhdlEndNamedElement;
import com.staremax.vhdlplug.psi.VhdlNamedElement;
import org.jetbrains.annotations.NotNull;

/**
 * Implemented by a custom language plugin to add annotations to files in the language.
 */
public class VhdlAnnotator implements Annotator {

    /**
     * Create annotation if endname does not match element's main name
     * @param holder annotations' container
     * @param element VhdlElement with endname
     */
    private void createHolderAnnotation(AnnotationHolder holder, VhdlEndNamedElement element) {
        final VhdlNamedElement namedElement = element.getMainName();
        final VhdlNamedElement endNamedElement = element.getEndName();
        if (endNamedElement != null) {
            final String name = namedElement.getName();
            final String endName = endNamedElement.getName();
            if (name != null && !name.equalsIgnoreCase(endName)) {
                TextRange range = endNamedElement.getTextRange();
                IntentionAction intent = new FixEndNameIntention(element);
                holder.createErrorAnnotation(range, "Expected name '" + name + "'").registerFix(intent);
            }
        }
    }

    /**
     * Annotates the specified PSI element.
     * It is guaranteed to be executed in non-reentrant fashion.
     * I.e there will be no call of this method for this instance before previous call get completed.
     * Multiple instances of the annotator might exist simultaneously, though.
     *
     * @param element to annotate.
     * @param holder  the container which receives annotations created by the plugin.
     */
    @Override
    public void annotate(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
        if (element instanceof VhdlEndNamedElement) {
            final VhdlEndNamedElement parent = (VhdlEndNamedElement) element;
            if (parent.getEndName() != null) {
                createHolderAnnotation(holder, parent);
            }
        }
    }
}
