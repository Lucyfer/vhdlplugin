package com.staremax.vhdlplug.refactoring;

import com.intellij.lang.refactoring.RefactoringSupportProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.impl.source.tree.LeafPsiElement;
import com.intellij.psi.tree.IElementType;
import com.staremax.vhdlplug.psi.VhdlArchitectureBody;
import com.staremax.vhdlplug.psi.VhdlEntityDeclaration;
import com.staremax.vhdlplug.psi.VhdlNamedElement;
import com.staremax.vhdlplug.psi.VhdlTypes;

public class VhdlRefactoringSupportProvider extends RefactoringSupportProvider {

    @Override
    public boolean isMemberInplaceRenameAvailable(PsiElement element, PsiElement context) {
        if (element instanceof VhdlNamedElement && context instanceof LeafPsiElement) {
            IElementType type = ((LeafPsiElement) context).getElementType();
            return type.equals(VhdlTypes.IDENTIFIER);
        }
        return false;
    }

    @Override
    public boolean isSafeDeleteAvailable(PsiElement element) {
        return element instanceof VhdlEntityDeclaration || element instanceof VhdlArchitectureBody;
    }
}
