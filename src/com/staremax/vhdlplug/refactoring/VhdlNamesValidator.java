package com.staremax.vhdlplug.refactoring;

import com.intellij.lang.refactoring.NamesValidator;
import com.intellij.openapi.project.Project;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class VhdlNamesValidator implements NamesValidator {
    private static final Set<String> KEYWORDS = new HashSet<String>();

    static {
        String[] keywords = new String[] {
                "ABS", "ACCESS", "AFTER", "ALIAS", "ALL", "AND", "ARCHITECTURE", "ARRAY", "ASSERT", "ATTRIBUTE",
                "BEGIN", "BLOCK", "BODY", "BUFFER", "BUS", "CASE", "COMPONENT", "CONFIGURATION", "CONSTANT", "DISCONNECT",
                "DOWNTO", "ELSE", "ELSIF", "END", "ENTITY", "EXIT", "FILE", "FOR", "FUNCTION", "GENERATE", "GENERIC",
                "GROUP", "GUARDED", "IF", "IMPURE", "IN", "INERTIAL", "INOUT", "IS", "LABEL", "LIBRARY", "LINKAGE",
                "LITERAL", "LOOP", "MAP", "MOD", "NAND", "NEW", "NEXT", "NOR", "NOT", "NULL", "OF", "ON", "OPEN", "OR",
                "OTHERS", "OUT", "PACKAGE", "PORT", "POSTPONED", "PROCEDURE", "PROCESS", "PURE", "RANGE", "RECORD",
                "REGISTER", "REJECT", "REM", "REPORT", "RETURN", "ROL", "ROR", "SELECT", "SEVERITY", "SIGNAL", "SHARED",
                "SLA", "SLL", "SRA", "SRL", "SUBTYPE", "THEN", "TO", "TRANSPORT", "TYPE", "UNAFFECTED", "UNITS",
                "UNTIL", "USE", "VARIABLE", "WAIT", "WHEN", "WHILE", "WITH", "XNOR", "XOR"
        };

        KEYWORDS.addAll(Arrays.asList(keywords));
    }

    @Override
    public boolean isIdentifier(String s, Project project) {
        return s != null && s.length() > 0 && Character.isLetter(s.charAt(0));
    }

    @Override
    public boolean isKeyword(String s, Project project) {
        return KEYWORDS.contains(s.toUpperCase());
    }
}
