package icons;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class VhdlIcons {
    private static Icon load(String path) {
        return IconLoader.getIcon(path, VhdlIcons.class);
    }

    public static final Icon VhdlBigIcon = load("/icons/vhdl.big.icon.png");
    public static final Icon VhdlIcon = load("/icons/vhdl.icon.png");
    public static final Icon VhdlImplIcon = load("/icons/vhdl-impl.icon.png");
    public static final Icon VhdlEntityIcon = load("/icons/vhdl-entity.icon.png");
    public static final Icon MakefileIcon = load("/icons/makefile.icon.png");
    public static final Icon DoxyfileIcon = load("/icons/doxyfile.icon.png");
}
